# WAIT
Evita hacer la fila. Solo con wait podras coordinar la visita.

## Comenzando 🚀

_El proyecto define directorios y el contenido para este proyecto. Con esto bastara tener una maquina virtual para arrancarla.

Estructura de directorios:
* bbdd : Script de bases de datos para arranque inicial como los datos
* images: Imagenes docker para la creacion de contenedores
* script: scripts batch para la generacion de automatizaciones
* source: Codigos del desarrollos los cuales bifurcan en microservicios y API

### Pre-requisitos 📋

Debido a que este proyecto esta pensado para correr en una maquina virtual, esta debe tener las siguientes caracteristicas:
* 2 cores
* 4gb de ram
* 15gb de disco

### Ejecucion 📋

La ejecucion de inicio para el ambientes es ejecutar el siguiente comando:
sh main.sh


## Administracion del ambiente 📋
Usuario: desarrollo
Password: 2021


### Administracion de Contenedores 📋
Se debe acceder a la ruta http://ip_maquina:9000 para la administracion de contenedores.
Usuario: desarrollo
Password: desarrollo2021 

### Comunicacion Mysql📋
Los accesos para el ingreso a bases de datos son:
* Mysql:
  Puerto:3306
  Usuario:wait
  Password: wait2021

* Mongodb:
  Puerto:27017
  Usuario:wait
  Password: wait2021


## Licencia 📄

Licencia pendiente de definicion, motivo por el cual no se puede copiar, distribuir como tampoco utilizar estos fuentes.





