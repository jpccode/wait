'user strict';
var dbConn = require('../../main/config/Conexion');
var conn = dbConn.connDB

//Objeto Usuario
var Usuario = function(usuario){
    this.usuario = usuario.usuario;
    this.password = usuario.password;
    this.rut = usuario.rut;
    this.dv = usuario.dv;
    this.nombre_principal = usuario.nombre_principal;
    this.nombre_secundario = usuario.nombre_secundario;
    this.apellido_principal = usuario.apellido_principal;
    this.apellido_secundario = usuario.apellido_secundario;
    this.fecha_nacimiento = usuario.fecha_nacimiento;
    this.direccion = usuario.direccion;
    this.correo = usuario.correo;
    //this.fecha_alta = '01-01-2020'; 
    this.numero_contacto = usuario.numero_contacto;

};

//CREAR NUEVO Usuario
Usuario.crearUsuario = function (newUsuario, result) {
    
    let sql = 'CALL sp_insertUsuario(?,?,?,?,?,?,?,?,?,?,?,?)';
    conn.query(sql, 
        [
            newUsuario.usuario, 
            newUsuario.password,
            newUsuario.rut,
            newUsuario.dv,
            newUsuario.nombre_principal,
            newUsuario.nombre_secundario,
            newUsuario.apellido_principal,
            newUsuario.apellido_secundario,
            newUsuario.fecha_nacimiento,
            newUsuario.direccion,
            newUsuario.correo,
            //newUsuario.fecha_alta,
            newUsuario.numero_contacto
        ],
        function (err, res) {
            console.log(newUsuario);
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });           
};

module.exports= Usuario;