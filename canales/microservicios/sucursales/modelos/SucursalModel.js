'user strict';
var dbConn = require('../../main/config/Conexion');
var conn = dbConn.connDB
//Objeto Sucursal
var Sucursal = function(suc){
    this.ID_SOCIO = suc.ID_SOCIO;
    this.NOMBRE_SUCURSAL = suc.NOMBRE_SUCURSAL;
    this.DIRECCION = suc.DIRECCION;
    this.NUMERO_CONTACTO = suc.NUMERO_CONTACTO;
    this.CORREO_CONTACTO = suc.CORREO_CONTACTO;
    this.pkconfig = suc.pkconfig;

};

//CREAR NUEVA SUCURSAL
Sucursal.crearSucursal = function (newSuc, result) {
    
    let sql = 'CALL sp_insertSucursal(?)';
    conn.query(sql, newSuc, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
            console.log("POST/ ", res);
        }
    });           
};

//OBTENER SUCURSALES POR ID DE SOCIO
Sucursal.getSucursales = function (id, result) {
    
    let sql = 'CALL sp_cargarInfoSucursales(?)';
    conn.query(sql, id, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
            console.log("GET /", id);
        }
    });   
};

//OBTENER UNA SUCURSAL POR SU ID
Sucursal.getSucursal = function (id, result) {
    
    let sql = 'CALL sp_getSucursal(?)';
    conn.query(sql, id, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });   
};

//OBTENER SUCURSAL POR PKCONFIG (PARA DIRECCION)
Sucursal.getDireccion = function (pkconfig, result) {
    
    let sql = 'CALL sp_getDireccionSuc(?)';
    conn.query(sql, pkconfig, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });   
};


//OBTENER TODOS LOS REGISTROS DE SUCURSALES
Sucursal.obtenerSucursales = function (result) {
    let sql = 'CALL sp_getAllSucursales()';
    conn.query(sql, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('Resultados: ', res);  
            result(null, res);
        }
    });   
};


//TODO

//ACTUALIZAR REGISTRO DE SUCURSAL
Sucursal.update = function(id, employee, result){
    conn.query("UPDATE employees SET first_name=?,last_name=?,email=?,phone=?,organization=?,designation=?,salary=? WHERE id = ?", [employee.first_name,employee.last_name,employee.email,employee.phone,employee.organization,employee.designation,employee.salary, id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }else{   
            result(null, res);
        }
    }); 
};

//ELIMINAR REGISTRO DE SUCURSAL
Sucursal.delete = function(id, result){
    let sql = 'CALL sp_deleteSucursal(?)';
    conn.query(sql, [id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log("DELETE /");
            result(null, res);
        }
    }); 
};

module.exports= Sucursal;