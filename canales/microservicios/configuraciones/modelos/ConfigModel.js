'user strict';
var dbConn = require('../../main/config/Conexion');
var conn = dbConn.connDB
//Objeto Config
var Config = function(config){
    this.aforo = config.aforo;
    this.dotacion_personal = config.dotacion_personal;
    this.tiempo_atraso = config.tiempo_atraso;
    this.config_horario = config.config_horario;
    this.pkconfig = config.pkconfig;
};

//CREAR NUEVA CONFIGURACION (SIEMPRE TIENE VALORES POR DEFECTO)
Config.crearConfig = function (newConfig, result) {
    
    let sql = 'CALL sp_insertConfig(?)';
    conn.query(sql, newConfig, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });           
};

//OBTENER CONFIGURACION POR PKCONFIG (IDSOCIO.IDSUCURSAL)
Config.getConfig = function (pkconfig, result) {
    
    let sql = 'CALL sp_getConfig(?)';
    conn.query(sql, pkconfig, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });   
};

//OBTENER TODOS LOS REGISTROS DE CONFIGURACION (SIN USO)
Config.obtenerConfigs = function (result) {
    let sql = 'CALL sp_getAllConfig()';
    conn.query(sql, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('Resultados: ', res);  
            result(null, res);
        }
    });   
};


//ACTUALIZAR REGISTRO DE CONFIGURACION
Config.updateConfig = function(id, config, result){
    let sql = 'CALL sp_updateConfig(?)';
    conn.query(sql, [id, config], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }else{  
            console.log("PUT /"); 
            result(null, res);
        }
    }); 
};

//ELIMINAR REGISTRO DE CONFIGURACION
Config.deleteConfig = function(id, result){
    let sql = 'CALL sp_deleteConfig(?)';
    conn.query(sql, [id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log("DELETE /");
            result(null, res);
        }
    }); 
};

module.exports= Config;