//ReservaController
//Contiene las operaciones CRUD para reservas, con mongoDB


var mongoose = require('mongoose');
var Reserva  = mongoose.model('Reserva');

//GET - API - Obtener todas las reservas
exports.obtenerReservas =  function(req, res) {
	Reserva.find({}, function(err, reservasRes) {
    if(err) {
		console.log(err);
	}else{
		console.log("Resultados: ", reservasRes);
		console.log('GET /reservas')
		res.json(reservasRes);
	}
	});
};

//GET - SOCIO - Obtener todas las reservas
exports.obtenerReservasSoc =  function(req, res) {
	let idSoc = req.params.idSocio;
	Reserva.find({'idUsuario':idSoc, 'idSucursal': 0}, function(err, reservasRes) {
    if(err) {
		console.log(err);
	}else{
		console.log("Resultados: ", reservasRes);
		console.log('GET /reservas')
		res.json(reservasRes);
	}
	});
};


//GET - SUCURSAL - Obtener todas las reservas
exports.obtenerReservasSuc =  function(req, res) {
	let idSoc = req.params.idSucursal;
	Reserva.find({'idSucursal': 0}, function(err, reservasRes) {
    if(err) {
		console.log(err);
	}else{
		console.log("Resultados: ", reservasRes);
		console.log('GET /reservas')
		res.json(reservasRes);
	}
	});
};


//GET - USUARIO - obtener reservas del usuario partir de su ID
exports.getReserva = function(req, res) {
	let idUser = req.params.idUsuario;
	let filtro;
	let action = req.query.action;
switch (action) {
	case 'historial': //reservas con estado != vigente
	filtro = {'idUsuario': idUser, 'idEstado': {$ne: 1}};
		break;
	case 'home': //solo reservas vigentes
	filtro = {'idUsuario': idUser, 'idEstado': 1};
		break;
	default: //todas las reservas del usuario
	filtro = {'idUsuario': idUser};
		break;
}

	Reserva.find(filtro, function(err, reservaRes) {
    if(err) return res.send(500, err.message);

    console.log('GET /reserva/' + idUser+'?action='+action);
		res.status(200).json(reservaRes);
	});
};

//POST - insertar documento de Reserva en la DB
exports.addReserva = function(req, res) {
	console.log('POST');
	console.log(req.body);

	var reservaNew = new Reserva({
		idUsuario:    req.body.idUsuario,
		idSocio: 	  req.body.idSocio,
		nombreSocio: 	  req.body.nombreSocio,
		idSucursal: 	req.body.idSucursal,
		nombreSucursal: 	  req.body.nombreSucursal,
		direccion: 	  req.body.direccion,
		fechaHora:  new Date(req.body.fechaHora),
		idEstado:   req.body.idEstado,
		nombreEstado: 	  req.body.nombreEstado,
		fechaCreacion:  new Date(req.body.fechaCreacion)
	});

	reservaNew.save(function(err, reservaNew) {
		if(err) return res.status(500).send(err.message);
    res.status(200).jsonp(reservaNew);
	});
};

//PUT - Modificar una reserva existente
exports.updateReserva = function(req, res) {
	console.log('PUT/ '+req.params.id);
	console.log(req.body);
	Reserva.findById(req.params.id, function(err, reservaNew) {
		reservaNew.fechaHora   = req.body.fechaHora;
		reservaNew.idEstado = req.body.idEstado;
		reservaNew.nombreEstado = req.body.nombreEstado;
		reservaNew.save(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200).jsonp(reservaNew);
		});
	});
};

//DELETE - Eliminar reserva (sin uso)
 exports.deleteReserva = function(req, res) {
	Reserva.findById(req.params.id, function(err, reserva) {
		reserva.remove(function(err) {
			if(err) return res.send(500, err.message);
      res.status(200);
		})
	});
};