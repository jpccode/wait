const express = require('express')
const router = express.Router()
const ReservaCtrl = require('../controladores/ReservaController');

// GET - Obtener todos los socios
router.get('/reservas', ReservaCtrl.obtenerReservas);

// POST - Crear un nuevo registro
router.post('/reservas', ReservaCtrl.addReserva);

// GET - Obtener un registro por su ID
router.get('/reserva/:idUsuario', ReservaCtrl.getReserva);

// GET - SOCIO - Obtener registros por su ID de socio
router.get('/reserva/socio/:idSocio', ReservaCtrl.getReserva);

// GET - SUCURSAL - Obtener registros por su ID de sucursal
router.get('/reserva/sucursal/:idSucursal', ReservaCtrl.getReserva);

// PUT - Actualizar registro por ID
router.put('/reserva/:id', ReservaCtrl.updateReserva);

// DELETE - Eliminar registro por ID
router.delete('/reserva/:id', ReservaCtrl.deleteReserva);

module.exports = router