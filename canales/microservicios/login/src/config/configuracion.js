function parametrosAmbiente(nombreParametro, required) {
    var parametroAmbiente = process.env[nombreParametro];
    if (!parametroAmbiente) {
        if (required)
            throw new Error("Falta variable de entorno: " + paramName);
        else return null;
    }

    return parametroAmbiente;
}
const microservicio = {
    directorio: __dirname + "/../",
    ambiente: parametrosAmbiente("AMBIENTE", false) || "DESA",
    port: parametrosAmbiente("PORT", false) || 3001
    //Ingresa aqui tus variables de ambiente


};

module.exports = {
    microservicio
};