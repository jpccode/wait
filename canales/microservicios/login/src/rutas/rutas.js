const { loginmysql } = require('../controladores/loginmysql.controlador.js');
const Ajv = require("ajv");
const configuracion = require("../config/configuracion");


nombreMicroservicio = "loginmysql";
version = "1.0";


function rutas(app) {
    const ajv = new Ajv()
    const urn = `/wait/ms/${version}/${nombreMicroservicio}/login`;
    app.post(urn, (req,res)=>loginmysql(req, res) );
    console.log("URI: http://localhost:" + configuracion.microservicio.port + urn);
}

module.exports = { rutas };
