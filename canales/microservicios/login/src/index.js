"use strict"

const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const { rutas } = require("./rutas/rutas");
const configuracion = require("./config/configuracion");
const cors = require('cors');

const app = express();

async function serverStart() {
    app.use(bodyParser.json());
    app.use(helmet());
    app.use(cors());
    rutas(app);

    let port = configuracion.microservicio.port;
    app.listen(port, () => {
        console.log("[index.js] Servidor corriendo en el puerto: " + port)
    });
}


async function cleanup() {
    // logger("index.js", "DEBUG", "Saliendo");
    process.exit(0);
}


process.on("SIGTERM", cleanup);
process.on("SIGINT", cleanup);
process.on("SIGHUP", cleanup);

serverStart();