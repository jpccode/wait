'use strict'
var mysql      = require('mysql');

const { config } = require('../config/configuracion');



const loginmysql = (req, res) => {

  let messageCode = '0000';
  let output='';
  let httpStatusCode = 200;
  const usuario = req.body.usuario;
  const password = req.body.password;

   console.log("Usuario ingresado: " + usuario);
   console.log("Password ingresado: " + password);
  try {
    let respuesta ='';
    let respuestaUsuario ='';
    let respuestaEjecutivo ='';
    var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'wait',
      password : 'wait2021',
      database : 'wait'
    });


    connection.connect();
    let query = `SELECT US.*, EJ.ID_SOCIO, SO.NOMBRE_SOCIO, EJ.ID_SUCURSAL, SUC.NOMBRE_SUCURSAL 
      FROM wait.USUARIO US 
      left join wait.EJECUTIVO EJ 
      ON US.ID = EJ.ID 
      left join wait.socio SO 
      ON EJ.ID_SOCIO = SO.ID
      lefT join wait.sucursal SUC
      ON EJ.ID_SUCURSAL = SUC.ID
      WHERE US.NOMBRE_USUARIO = '${usuario}' and US.PASSWORD = '${password}'`;
    console.log('QUERY: ' + query);
    
    connection.query(query, function (error, results, fields) {
      if (error) throw error;
      // console.log('The solution is: ', results[0]);
      console.log("numero de respuestas: " + results.length);

      if(results.length == 0){

        respuesta = {
          Error: "Usuario o password invalido"
        }
      }else{
        
         respuestaUsuario= {          
          ID: results[0].ID,
          RUT: results[0].RUT,
          DV: results[0].DV,
          NOMBRE_PRINCIPAL: results[0].NOMBRE_PRINCIPAL,
          NOMBRE_SECUNDARIO: results[0].NOMBRE_SECUNDARIO,
          APELLIDO_PRINCIPAL: results[0].APELLIDO_PRINCIPAL,
          APELLIDO_SECUNDARIO: results[0].APELLIDO_SECUNDARIO,
          FECHA_NACIMIENTO: results[0].FECHA_NACIMIENTO,
          DIRECCION: results[0].DIRECCION,
          CORREO: results[0].CORREO,
          FECHA_ALTA: results[0].FECHA_ALTA,
          FECHA_BAJA: results[0].FECHA_BAJA,
          NUMERO_CONTACTO: results[0].NUMERO_CONTACTO,
          TIPO_USUARIO: results[0].TIPO_USUARIO
        }
        if (respuestaUsuario.TIPO_USUARIO == 'USUARIO'){
          respuesta = respuestaUsuario
          console.log("Perfil ingresado: " + respuesta.TIPO_USUARIO);
        } else {
          if (respuestaUsuario.TIPO_USUARIO == 'EJECUTIVO'){
            respuestaEjecutivo= {          
              ID_SOCIO: results[0].ID_SOCIO,
              NOMBRE_SOCIO: results[0].NOMBRE_SOCIO,
              ID_SUCURSAL: results[0].ID_SUCURSAL,
              NOMBRE_SUCURSAL: results[0].NOMBRE_SUCURSAL
            }
            respuesta = {
              ...respuestaUsuario,
              ...respuestaEjecutivo
            }
          }
        }
        console.log("Perfil ingresado: " + respuesta.TIPO_USUARIO);
      }
      
      
      output = respuesta;
      messageCode = '500';
      httpStatusCode = 200;
      return res.status(httpStatusCode).send(output);

    });

    connection.end();
  } catch (error) {
    respuesta = {
      ERROR: "001",
      DETALLE: "Error no contralado en login",
      ERROR_CATCH: error
    }
    output = respuesta;
    messageCode = '200';
    httpStatusCode = 200;
    return res.status(httpStatusCode).send(output);
  }
}

module.exports = {
  loginmysql
};
