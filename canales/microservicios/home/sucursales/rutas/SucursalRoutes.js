const express = require('express')
const router = express.Router()
const SucursalController = require('../controladores/SucursalController');

// GET - Obtener todas las sucursales
router.get('/sucursales', SucursalController.obtenerSucursales);

// POST - Crear una nueva sucursal
router.post('/sucursales', SucursalController.crearSucursal);

// GET - Obtener sucursales por ID de socio (requiere parámetro)
router.get('/sucursales/:ID_SOCIO', SucursalController.getSucursales);

// GET - Obtener sucursal por ID
router.get('/sucursal/:id', SucursalController.getSucursal);

// GET - Obtener sucursal por su PKCONFIG
router.get('/infosucursal/:pkconfig', SucursalController.getDireccion);

// PUT - Actualizar registro por ID
router.put('/sucursal/:id', SucursalController.update);

// DELETE - Eliminar registro por ID
router.delete('/sucursal/:id', SucursalController.delete);

module.exports = router