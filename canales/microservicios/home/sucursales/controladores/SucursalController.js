'use strict';

const Sucursal = require('../modelos/SucursalModel');

//OBTENER TODOS LOS REGISTROS DE SUCURSALES
exports.obtenerSucursales = function(req, res) {
    Sucursal.obtenerSucursales(function(err, sucs) {
    console.log('GET /')
    if (err)
    res.send(err);
    console.log('res', sucs);
    res.send(sucs);
  });
};


// CREAR UNA NUEVA SUCURSAL
exports.crearSucursal = function(req, res) {
    const newSucursal = new Sucursal(req.body);

    //handles null error 
   if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
    }else{
        Sucursal.crearSucursal(newSucursal, function(err, suc) {
            if (err)
            res.send(err);
            res.json({error:false,message:"Registro creado exitosamente"});
            console.log('POST/ ', suc);
        });
    }
};

// OBTENER SUCURSALES POR ID DE SOCIO
exports.getSucursales = function(req, res) {
    Sucursal.getSucursales(req.params.ID_SOCIO, function(err, socio) {
        let idSocio = req.params.ID_SOCIO;
	let action = 'all';
        if (err)
        res.send(err);
        res.json(socio);
        console.log('GET /sucursales/' + idSocio+'?action='+action);
    });
};


// OBTENER UNA SUCURSAL POR SU ID
exports.getSucursal = function(req, res) {
    Sucursal.getSucursal(req.params.id, function(err, socio) {
        if (err)
        res.send(err);
        res.json(socio);
    });
};

// OBTENER UNA SUCURSAL POR PKCONFIG
exports.getDireccion = function(req, res) {
    Sucursal.getDireccion(req.params.pkconfig, function(err, socio) {
        if (err)
        res.send(err);
        res.json(socio);
    });
};

//TODO

exports.update = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
    }else{
        Sucursal.update(req.params.id, new Sucursal(req.body), function(err, employee) {
            if (err)
            res.send(err);
            res.json({ error:false, message: 'Registro actualizado exitosamente' });
        });
    }
  
};

exports.delete = function(req, res) {
    Sucursal.delete( req.params.id, function(err, employee) {
    if (err)
    res.send(err);
    res.json({ error:false, message: 'Registro eliminado exitosamente' });
  });
};