'user strict';
var dbConn = require('./../../config/Conexion');
var conn = dbConn.connDB
//Objeto Socio
var Socio = function(socio){
    this.nombre_socio = socio.nombre_socio;
    this.rut = socio.rut;
    this.dv = socio.dv;
    this.direccion = socio.direccion;
    this.numero_contacto = socio.numero_contacto;
    this.numero_emergencia = socio.numero_emergencia;
    this.correo_electronico = socio.correo_electronico;
    this.nombre_holding = socio.nombre_holding;
    //this.pkconfig = socio.pkconfig;
};

//CREAR NUEVO SOCIO
Socio.crearSocio = function (newSocio, result) {
    
    let sql = 'CALL sp_insertSocio(?,?,?,?,?,?,?,?)';
    conn.query(sql,
        [ 
            newSocio.nombre_socio,
            newSocio.rut,
            newSocio.dv,
            newSocio.direccion,
            newSocio.numero_contacto,
            newSocio.numero_emergencia,
            newSocio.correo_electronico,
            newSocio.nombre_holding
        ],
        function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });           
};

//OBTENER SOCIO POR ID
Socio.getSocio = function (id, result) {
    
    let sql = 'CALL sp_getSocio(?)';
    conn.query(sql, id, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });   
};

//OBTENER SOCIO POR PKCONFIG (PARA DIRECCION)
Socio.getDireccion = function (pkconfig, result) {
    
    let sql = 'CALL sp_getDireccionSoc(?)';
    conn.query(sql, pkconfig, function (err, res) {             
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });   
};

//OBTENER TODOS LOS REGISTROS DE SOCIOS
Socio.obtenerSocios = function (result) {
    let sql = 'CALL sp_cargarInfoSocios()';
    conn.query(sql, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('Resultados: ', res);  
            result(null, res);
        }
    });   
};


//TODO

//ACTUALIZAR REGISTRO DE SOCIO
Socio.update = function(id, employee, result){
    conn.query("UPDATE employees SET first_name=?,last_name=?,email=?,phone=?,organization=?,designation=?,salary=? WHERE id = ?", [employee.first_name,employee.last_name,employee.email,employee.phone,employee.organization,employee.designation,employee.salary, id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }else{   
            result(null, res);
        }
    }); 
};

//ELIMINAR REGISTRO DE SOCIO
Socio.delete = function(id, result){
    let sql = 'CALL sp_delete_socio(?)';
    conn.query(sql, [id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log("DELETE /");
            result(null, res);
        }
    }); 
};

module.exports= Socio;