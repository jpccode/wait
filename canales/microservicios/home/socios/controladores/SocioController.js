'use strict';

const Socio = require('../modelos/SocioModel');

exports.obtenerSocios = function(req, res) {
    Socio.obtenerSocios(function(err, socios) {
    console.log('GET /')
    if (err)
    res.send(err);
    console.log('res', socios);
    res.send(socios);
  });
};


exports.crearSocio = function(req, res) {
    const newSocio = new Socio(req.body);

    //handles null error 
   if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
    }else{
        Socio.crearSocio(newSocio, function(err, socio) {
            if (err)
            res.send(err);
            res.json({error:false,message:"Socio creado exitosamente",data:socio});
        });
    }
};


exports.getSocio = function(req, res) {
    Socio.getSocio(req.params.id, function(err, socio) {
        if (err)
        res.send(err);
        res.json(socio);
    });
};

exports.getDireccion = function(req, res) {
    Socio.getDireccion([req.params.pkconfig, req.params.pkconfig], function(err, socio) {
        if (err)
        res.send(err);
        res.json(socio);
    });
};


//TODO


exports.update = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
    }else{
        Socio.update(req.params.id, new Socio(req.body), function(err, employee) {
            if (err)
            res.send(err);
            res.json({ error:false, message: 'Registro actualizado exitosamente' });
        });
    }
  
};


exports.delete = function(req, res) {
    Socio.delete( req.params.id, function(err, employee) {
    if (err)
    res.send(err);
    res.json({ error:false, message: 'Registro eliminado exitosamente' });
  });
};