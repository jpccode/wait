const express = require('express')
const router = express.Router()
const SocioController = require('../controladores/SocioController');

// GET - Obtener todos los socios
router.get('/socios', SocioController.obtenerSocios);

// POST - Crear un nuevo socio
router.post('/socios', SocioController.crearSocio);

// GET - Obtener un socio por su ID
router.get('/socio/:id', SocioController.getSocio);

// GET - Obtener un socio por su PKCONFIG
router.get('/infosocio/:pkconfig', SocioController.getDireccion);

// PUT - Actualizar registro por ID
router.put('/socio/:id', SocioController.update);

// DELETE - Eliminar registro por ID
router.delete('/socio/:id', SocioController.delete);

module.exports = router