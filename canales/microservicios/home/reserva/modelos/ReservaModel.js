const { Collection } = require("mongoose");

exports = module.exports = function(app, mongoose) {

	var reservaSchema = new mongoose.Schema({
		idUsuario: 		{ type: Number },
		nombreUsuario: {type: String},
		apellidoUsuario: {type: String},
		idSocio: 	{ type: Number },
		nombreSocio: 	{ type: String },
		idSucursal: 	{ type: Number, default: 0 },
		nombreSucursal: 	{ type: String, default: '' },
		direccion: 	{ type: String },
		fechaHora:  	{ type: Date, default: Date.now },
		idEstado: 	{ type: Number, default: 1 },
		nombreEstado: 	{ type: String, default: 'Vigente' },
		fechaCreacion: 	{ type: Date, default: Date.now }
	}, {Collection: 'Reserva'}, );

	mongoose.model('Reserva', reservaSchema, 'Reserva');

};
