const express = require('express')
const router = express.Router()
const ReservaCtrl = require('../controladores/ReservaController');

// GET - Obtener todas las reservas de todos los socios
router.get('/reservas', ReservaCtrl.obtenerReservas);

// POST - Crear un nuevo socio
router.post('/reservas', ReservaCtrl.addReserva);

// GET - Obtener reservas por ID de usuario
router.get('/reserva/:idUsuario', ReservaCtrl.getReserva);

// GET - Obtener todos las reservas por id Socio
router.get('/reserva/socio/:idSocio', ReservaCtrl.getReservaSocio);

// GET - Obtener todos las reservas por id Socio e id Sucursal
router.get('/reserva/socio/:idSocio/sucursal/:idSucursal', ReservaCtrl.getReservaSucursal);

// PUT - Actualizar registro por ID
router.put('/reserva/:id', ReservaCtrl.updateReserva);

// DELETE - Eliminar registro por ID
router.delete('/reserva/:id', ReservaCtrl.deleteReserva);

module.exports = router