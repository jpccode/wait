const express = require('express')
const router = express.Router()
const UsuarioController = require('../controladores/UserController');



var rutaCrearUsuario='/usuarios/crear';
var rutaGetUsuario='/usuarios/obtener';

// POST - Crear un nuevo usuario
router.post(rutaCrearUsuario, UsuarioController.crearUsuario);


// GET - Obtener datos usuario por id
router.get('/usuarios/obtener/:id', UsuarioController.obtenerUsuario);

module.exports = router