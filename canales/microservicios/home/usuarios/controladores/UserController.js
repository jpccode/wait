'use strict';

const Usuario = require('../modelos/UserModel');

exports.crearUsuario = function(req, res) {
  const newUsuario = new Usuario(req.body);

  //handles null error 
 if(req.body.constructor === Object && Object.keys(req.body).length === 0){
      res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
  }else{
      Usuario.crearUsuario(newUsuario, function(err, usuario) {
        if(err){
            if (err.code == "ER_DUP_ENTRY"){
                //res.json({error:true,message:"Usuario ya se encuentra registrado",data:usuario});
                res.status(200).send({
                    'status': '0',
                    'success': false,
                    'title': 'Error al registrar Usuario',
                    'message': 'El usuario ingresado ya se encuentra registrado.'
                });
            }
            else {
                res.json({error:true,err});
                //res.send(err);
            }
        }else{
            res.status(201).send({
                'status': '1',
                'success': true,
                'title': 'Usuario Registrado',
                'message': 'El usuario se ha registrado exitosamente.'
            //res.json({error:false,message:"Usuario creado exitosamente",data:usuario});
            });
          }
      });
  }
}


exports.obtenerUsuario = function(req, res) {
    Usuario.obtenerUsuario(req.params.id, function(err, usuario) {
        if (err)
        res.send(err);
        res.status(200).jsonp(usuario);
        //res.jsonp(usuario);
    });
};