'user strict';

const mysql = require('mysql2');
const config = require('./Config');
const mongoose = require('mongoose');
//Conexión a MYSQL

  var conn = config.mysqlconfig;
  const connDB = mysql.createConnection(conn);
 connDB.connect(function(err) {
  if (err) throw err;
  console.log("Conectado a MYSQL");
});

module.exports = {
  connDB: mysql.createConnection(conn)
};

let conectarMongo = function(){
      //var obj = new config.mongoconfig();
     var mongoUrl = 'mongodb://'+config.mongoconfig.mongohost+':'+config.mongoconfig.mongoport+'/'+config.mongoconfig.mongodb;
         mongoose.connect(mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true});
        mongoose.connection
     .once('open', () => console.log('Conectado a MongoDB'))
     .on('error', (error) => {
      console.log('cadena conexión:', mongoUrl);
        console.log('error de conexión con MongoDB:', error);
        
})
};
module.exports.conectarMongo = conectarMongo;