const express = require('express')
const router = express.Router()
const ConfigController = require('../controladores/ConfigController');

// GET - Obtener todos los config
router.get('/configuraciones/', ConfigController.obtenerConfigs);

// POST - Crear un nuevo config
router.post('/configuraciones', ConfigController.crearConfig);

// GET - Obtener un config por PKCONFIG (IDSOCIO.IDSUCURSAL)
router.get('/config/:PKCONFIG', ConfigController.getConfig);

// PUT - Actualizar registro por PKCONFIG (IDSOCIO.IDSUCURSAL)
router.put('/config/:PKCONFIG', ConfigController.updateConfig);

// DELETE - Eliminar registro por PKCONFIG (IDSOCIO.IDSUCURSAL)
router.delete('/config/:PKCONFIG', ConfigController.deleteConfig);

module.exports = router