'use strict';

const Config = require('../modelos/ConfigModel');

exports.obtenerConfigs = function(req, res) {
    Config.obtenerConfigs(function(err, configs) {
    console.log('GET /')
    if (err)
    res.send(err);
    console.log('res', configs);
    res.send(configs);
  });
};


exports.crearConfig = function(req, res) {
    const newConfig = new Config(req.body);

    //handles null error 
   if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
    }else{
        Config.crearConfig(newConfig, function(err, config) {
            if (err)
            res.send(err);
            res.json({error:false,message:"Registro creado exitosamente",data:config});
        });
    }
};


exports.getConfig = function(req, res) {
    Config.getConfig(req.params.PKCONFIG, function(err, config) {
        if (err)
        res.send(err);
        res.json(config);
    });
};


exports.updateConfig = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Por favor ingrese todos los datos' });
    }else{
        Config.update(req.params.pkconfig, new Config(req.body), function(err, config) {
            if (err)
            res.send(err);
            res.json({ error:false, message: 'Registro actualizado exitosamente' });
        });
    }
  
};


exports.deleteConfig = function(req, res) {
    Config.delete( req.params.pkconfig, function(err, employee) {
    if (err)
    res.send(err);
    res.json({ error:false, message: 'Registro eliminado exitosamente' });
  });
};