

var dbConn = require('./config/Conexion');
var express         = require("express"),
    app             = express(),
    bodyParser      = require("body-parser"),
    methodOverride  = require("method-override"),
    mongoose        = require('mongoose');
    const cors = require('cors');
     //var mongoUrl = 'mongodb://'+mongoClass.mongohost+':'+mongoClass.mongoport+'/'+mongoClass.mongodb;

// Middlewares
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(methodOverride());
app.use(cors());

// Example Route
var router = express.Router();
router.get('/', function(req, res) {
  res.send("Hola Mundo");
});
app.use(router);

//SCHEMA
var models  = require('./reserva/modelos/ReservaModel')(app, mongoose);

// API routes
const rutaReserva = require('./reserva/rutas/ReservaRoutes');
const rutaSocio = require('./socios/rutas/SocioRoutes');
const rutaSucursal = require('./sucursales/rutas/SucursalRoutes');
const rutaUsuario = require('./usuarios/rutas/UserRoutes');
const rutaConfig = require('./configuraciones/rutas/ConfigRoutes');

app.use('/wait/', rutaReserva);
app.use('/wait/', rutaSocio);
app.use('/wait/', rutaSucursal);


app.use('/wait/', rutaReserva);
app.use('/wait/', rutaSocio);
app.use('/wait/', rutaUsuario);
app.use('/wait/', rutaConfig);

// Start server
var port = 3000;
app.listen(port, function() {
  console.log("Servidor corriendo en http://localhost:"+port);
  //connectToMongo();
  dbConn.conectarMongo();
});
