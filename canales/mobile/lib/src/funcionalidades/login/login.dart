// import 'dart:js';
//import 'dart:js';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:wait/src/funcionalidades/usuario/RegistrarUsuario.dart';
import 'dart:async';
import '../configuration/config.dart';
import 'dart:convert';

import '../home/home_page.dart';
import '../home/Home_Socio.dart';
import '../home/Home_Usuario.dart';

class Login extends StatefulWidget {
  @override
  createState() {
    return _LoginState();
  }
}

DatosUsuario usuarioEncontrado = new DatosUsuario();
DatosUsuario usuarioLogeado = new DatosUsuario();

class _LoginState extends State<Login> {
  final _estiloTexto = new TextStyle(fontSize: 30);

  String _nombre = '';
  String _password = '';
  String _validaUsuario = '';
  String _mensajeError = '.';

  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BIENVENIDO WAIT"),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _ingresoUsuario(),
          _ingresoPassword(),
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
              Widget>[
            ElevatedButton(
              onPressed: () async {
                DatosUsuario usuario = await validaUsuario(_nombre, _password);

                print("Ejecuto Login con: Usuario:" +
                    _nombre +
                    " y password:" +
                    _password);
                String tipoUsuarioLogeado = (usuario.tipoUsuario).toString();
                print("PERFIL DE USUARIO ES: " +
                    (usuario.tipoUsuario).toString());
                switch (tipoUsuarioLogeado) {
                  case "EJECUTIVO":
                    {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Home_Socio(usuario)));
                    }
                    break;
                  case "USUARIO":
                    {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Home_Usuario(usuario)));
                    }
                    break;
                  default:
                    {
                      _mensajeError =
                          'Usuario o contraseña no válidos. Intente nuevamente.';
                      setState(() {});
                    }
                    break;
                }
              },
              child: Text('Ingresar'),
            ),
            ElevatedButton(
              onPressed: () async {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (ctx) => RegistrarUsuario()));
              },
              child: Text('Crear Usuario'),
            ),
          ]),
          Center(
            child: Text(_mensajeError),
          ),
        ],
      ),
    );
  }

  Widget _ingresoUsuario() {
    return TextField(
      autofocus: true,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          hintText: "Ingrese Nombre Usuario",
          labelText: "Usuario",
          suffixIcon: Icon(Icons.accessibility_new),
          icon: Icon(Icons.account_box)),
      onChanged: (input) {
        _nombre = input;
      },
    );
  }

  Widget _ingresoPassword() {
    return TextField(
      autofocus: true,
      obscureText: true,
      textCapitalization: TextCapitalization.characters,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          hintText: "Ingrese su contraseña",
          labelText: "Contraseña",
          suffixIcon: Icon(Icons.accessibility_new),
          icon: Icon(Icons.account_box)),
      onChanged: (input) {
        _password = input;
      },
    );
  }
}

Future<DatosUsuario> validaUsuario(nombre, password) async {
  usuarioLogeado = await validaCredencial(nombre, password);
  return usuarioLogeado;
}

Future<DatosUsuario> validaCredencial(String usuario, String password) async {
  String uri = MS_LOGIN_URL + MS_LOGIN_URN;
  print("URI POST: " + uri);
  final response = await http.post(
    Uri.parse(uri),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'usuario': usuario,
      'password': password,
    }),
  );

  if (response.statusCode == 200) {
    usuarioEncontrado = DatosUsuario.fromJson(jsonDecode(response.body));
    print("RESPUESTA REST: " + response.body + "\n");
    return usuarioEncontrado;
  } else {
    throw Exception('Falla REST al consultar usuario');
  }
}

class DatosUsuario {
  final int? id;
  final int? rut;
  final String? dv;
  final String? nombrePrincipal;
  final String? nombreSecundario;
  final String? apellidoPrincipal;
  final String? apellidoSecundario;
  final String? fechaNacimiento;
  final String? direccion;
  final String? correo;
  final String? fechaAlta;
  final String? fechaBaja;
  final String? numeroContacto;
  final String? tipoUsuario;
  final int? idSocio;
  final String? nombreSocio;
  final int? idSucursal;
  final String? nombreSucursal;

  DatosUsuario(
      {this.id,
      this.rut,
      this.dv,
      this.nombrePrincipal,
      this.nombreSecundario,
      this.apellidoPrincipal,
      this.apellidoSecundario,
      this.fechaNacimiento,
      this.direccion,
      this.correo,
      this.fechaAlta,
      this.fechaBaja,
      this.numeroContacto,
      this.tipoUsuario,
      this.idSocio,
      this.nombreSocio,
      this.idSucursal,
      this.nombreSucursal});

  factory DatosUsuario.fromJson(Map<String, dynamic> json) {
    return DatosUsuario(
        id: json['ID'],
        rut: json['RUT'],
        dv: json['DV'],
        nombrePrincipal: json['NOMBRE_PRINCIPAL'],
        nombreSecundario: json['NOMBRE_SECUNDARIO'],
        apellidoPrincipal: json['APELLIDO_PRINCIPAL'],
        apellidoSecundario: json['APELLIDO_SECUNDARIO'],
        fechaNacimiento: json['FECHA_NACIMIENTO'],
        direccion: json['DIRECCION'],
        correo: json['CORREO'],
        fechaAlta: json['FECHA_ALTA'],
        fechaBaja: json['FECHA_BAJA'],
        numeroContacto: json['NUMERO_CONTACTO'],
        tipoUsuario: json['TIPO_USUARIO'],
        idSocio: json['ID_SOCIO'],
        nombreSocio: json['NOMBRE_SOCIO'],
        idSucursal: json['ID_SUCURSAL'],
        nombreSucursal: json['NOMBRE_SUCURSAL']);
  }
}
