//import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:wait/src/funcionalidades/home/Home_Socio.dart';
import 'dart:convert';
import '../configuration/config.dart';
import 'package:flutter/services.dart';

import 'package:intl/intl.dart';
import 'package:wait/src/funcionalidades/login/login.dart';
import 'package:wait/src/funcionalidades/fila/fila.dart';

class DetalleAtencion extends StatefulWidget {
  final int? index;
  final DatosUsuario? usuario;
  final List? lista;
  DetalleAtencion({this.index, this.lista, this.usuario});

  @override
  _DetalleAtencionState createState() => _DetalleAtencionState();
}

class _DetalleAtencionState extends State<DetalleAtencion> {
  @override
  void initState() {
    super.initState();
  }

  RespuestaActualizarReserva respuestaRecbida =
      new RespuestaActualizarReserva();

  String separaFecha(String date) {
    var formatoFecha = DateFormat("dd-MM-yyyy");
    DateTime temp = DateTime.parse(date);
    var fecha = formatoFecha.format(temp);
    return fecha;
  }

  String separaHora(String date) {
    var formatoHora = DateFormat("HH:mm:ss");
    DateTime temp = DateTime.parse(date);
    var hora = formatoHora.format(temp);
    return hora;
  }

  bool isSwitched = false;
  String? newid;
  String? newDate;
  String? title;
  String? descr;
  String? estadoReserva;
  int? idEstado;
  bool consolidado = false;
  bool iniciado = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: new Column(
        children: <Widget>[
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Iniciar Atención: ',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(width: 20.0),
              new Visibility(
                visible: iniciado,
                child: new FloatingActionButton(
                  onPressed: () {
                    newid = widget.lista![widget.index!]['_id'].toString();
                    newDate =
                        widget.lista![widget.index!]['fechaHora'].toString();
                    estadoReserva = 'Asignada';
                    idEstado = 6;
                    actualizaReserva().then((val) => setState(() {
                          RespuestaActualizarReserva respuesta = val;
                          if (respuesta.nombreEstado == 'Asignada') {
                            title = 'Atención Iniciada';
                            descr = 'Se inicia atención con éxito';
                            consolidado = true;
                            iniciado = false;
                            showAlertDialog(context);
                            //Navigator.of(context)
                            //  .push(MaterialPageRoute(builder: (ctx) => Login()));
                          } else {
                            title = 'Error';
                            descr = 'No es posible iniciar atención';
                            showAlertDialog(context);
                          }
                        }));

                    //Navigator.pop(context, 'Reserva actualizada exitosamente.');
                  }, // Refer step 3
                  child: Icon(
                    Icons.schedule_send_rounded,
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Fin Atención: ',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(width: 20.0),
              new Visibility(
                visible: consolidado,
                child: new FloatingActionButton(
                  onPressed: () {
                    newid = widget.lista![widget.index!]['_id'].toString();
                    newDate =
                        widget.lista![widget.index!]['fechaHora'].toString();
                    estadoReserva = 'Consolidada';
                    idEstado = 2;
                    actualizaReserva().then((val) => setState(() {
                          RespuestaActualizarReserva respuesta = val;
                          if (respuesta.nombreEstado == 'Consolidada') {
                            title = 'Atención Finalizada';
                            descr = 'Se finaliza atención con éxito';
                            //showAlertDialog(context);
                            Navigator.pop(context, true);
                          } else {
                            title = 'Error';
                            descr = 'No es posible finalizar atención';
                            showAlertDialog(context);
                          }
                        }));

                    //Navigator.pop(context, 'Reserva actualizada exitosamente.');
                  }, // Refer step 3
                  child: Icon(
                    Icons.save_outlined,
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 20.0),
          //new Visibility(visible: true, child: new Text(newid!)),
        ],
      ),
    );
  }

  int? idUser = 1;
  late DateTime fechaNew;

  Future<RespuestaActualizarReserva> actualizaReserva() async {
    var uri = nodeHost + nodePort + rutaReserva + newid!;
    print(uri);
    print(newDate);
    //String motivoAnular = 'Anulada por ejecutivo ';

    var body = json.encode({
      "idEstado": idEstado,
      "nombreEstado": estadoReserva,
      "fechaHora": newDate,
    });
    try {
      final http.Response response = await http.put(Uri.parse(uri),
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: body);
      respuestaRecbida =
          RespuestaActualizarReserva.fromJson(jsonDecode(response.body));
      return respuestaRecbida;
    } catch (e) {
      print(e);
      throw Exception('Error de Comunicación con API REST');
    }
  }

  ///////Alert Dialog
  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    // set up the AlertDialog

    AlertDialog alert = AlertDialog(
      title: Text(title.toString()),
      content: Text(descr.toString()),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class RespuestaActualizarReserva {
  final String? id;
  final String? idEstado;
  final String? nombreEstado;

  RespuestaActualizarReserva({this.id, this.idEstado, this.nombreEstado});

  factory RespuestaActualizarReserva.fromJson(Map<String, dynamic> json) {
    return RespuestaActualizarReserva(
      id: json['_id'],
      idEstado: json['idestado'],
      nombreEstado: json['nombreEstado'],
    );
  }
}
