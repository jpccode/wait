import 'package:flutter/material.dart';
import 'package:wait/src/funcionalidades/login/login.dart';

class DetalleHistorialAtencion extends StatefulWidget {
  final int? index;
  final List? lista;
  final DatosUsuario? usuario;

  DetalleHistorialAtencion({this.index, this.lista, this.usuario});

  @override
  _DetalleHistorialAtencionState createState() =>
      _DetalleHistorialAtencionState();
}

class _DetalleHistorialAtencionState extends State<DetalleHistorialAtencion> {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Column(
        children: <Widget>[
          new Text(
            widget.lista![widget.index!]['nombreUsuario'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          new Text(
            widget.lista![widget.index!]['apellidoUsuario'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          new Text(
            widget.lista![widget.index!]['direccion'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          new Text(
            widget.lista![widget.index!]['fechaHora'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          new Text(
            widget.lista![widget.index!]['nombreEstado'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
        ],
      ),
    );
  }
}
