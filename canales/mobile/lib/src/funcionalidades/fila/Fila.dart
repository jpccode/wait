import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wait/src/funcionalidades/login/login.dart';
import 'package:wait/src/funcionalidades/fila/HistorialAtencion.dart';
import 'package:wait/src/funcionalidades/reserva/CrearReserva.dart';
import 'package:wait/src/funcionalidades/reserva/PopupContent.dart';
import 'package:wait/src/funcionalidades/reserva/PopupView.dart';
import 'package:wait/src/funcionalidades/reserva/DetalleVigente.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:wait/src/funcionalidades/fila/DetalleAtencion.dart';
import '../configuration/config.dart';

class Fila extends StatefulWidget {
  final DatosUsuario? usuario;
  Fila(this.usuario);

  @override
  _FilaState createState() => _FilaState();
}

List? fechas;

class _FilaState extends State<Fila> {
  Future<List> obtenerReservasSucursal(String action) async {
    final int idSocio = widget.usuario!.idSocio!;
    final int idSucursal = widget.usuario!.idSucursal!;
    print(nodeHost +
        nodePort +
        rutaReservaSocio +
        idSocio.toString() +
        rutaSucursal +
        idSucursal.toString() +
        '?action=' +
        action);
    try {
      final response = await http.get(Uri.parse(nodeHost +
          nodePort +
          rutaReservaSocio +
          idSocio.toString() +
          rutaSucursal +
          idSucursal.toString() +
          '?action=' +
          action));
      List data = json.decode(response.body);

      return data;
    } catch (e) {
      print('Exception = ' + e.toString());
      throw Exception(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        child: new Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                'Bienvenido,  ' + widget.usuario!.nombrePrincipal!,
                style: TextStyle(fontSize: 18),
              ),
              Container(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Socio:  ' + widget.usuario!.nombreSocio!,
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        ' -  Sucursal:  ' + widget.usuario!.nombreSucursal!,
                        style: TextStyle(fontSize: 18),
                      )
                    ]),
              ),
              SizedBox(
                height: 280.0,
                child: new FutureBuilder<List>(
                  future: obtenerReservasSucursal('home'),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) print(snapshot.error);
                    return snapshot.hasData
                        ? new ElementoLista(snapshot.data, widget.usuario)
                        : new Center(
                            child: new CircularProgressIndicator(),
                          );
                  },
                ),
              ),
              _crearBotones(),
            ],
          ),
        ),
      ),
    );
  }

  // ignore: unused_element
  Widget _crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(width: 20.0),
        FloatingActionButton(
          heroTag: "btn1",
          child: Icon(Icons.add),
          onPressed: () {
            print("Nueva reserva");
            setState(() {});
            //consultaHistorial();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CrearReserva(widget.usuario)));
            // _navigateAndDisplaySelection(context);
          },
        ),
        SizedBox(width: 20.0),
        FloatingActionButton(
          heroTag: "btn2",
          child: Icon(Icons.schedule),
          onPressed: () {
            print("Historial");
            setState(() {});
            //consultaHistorial();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => HistorialAtencion(widget.usuario!)));
          },
        ),
        SizedBox(width: 20.0),
/*         FloatingActionButton(
          heroTag: "btn3",
          child: Icon(Icons.person),
          onPressed: () {
            print("En construccion");
            setState(() {});
            //consultaHistorial();
            //Navigator.push(
            //  context, MaterialPageRoute(builder: (context) => historial()));
          },
        ), */
      ],
    );
  }
}

class ElementoLista extends StatelessWidget {
  final List? lista;
  int? index = 0;
  String? id;
  DatosUsuario? usuario;
  ElementoLista(this.lista, this.usuario);

  String separaFecha(String date) {
    var formatoFecha = DateFormat("dd-MM-yyyy");
    DateTime temp = DateTime.parse(date);
    var fecha = formatoFecha.format(temp);
    return fecha;
  }

  String separaHora(String date) {
    var formatoHora = DateFormat("HH:mm");
    DateTime temp = DateTime.parse(date);
    var hora = formatoHora.format(temp);
    return hora;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ListView.builder(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemCount: lista == null ? 0 : lista!.length,
      itemBuilder: (context, posicion) {
        return new Container(
            padding: EdgeInsets.all(2.0),
            child: new GestureDetector(
              child: new SizedBox(
                  child: new Card(
                color: Colors.deepPurple[100],
                child: new Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      Text(
                        lista![posicion]['nombreUsuario'],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 28),
                      ),
                      Text(
                        lista![posicion]['apellidoUsuario'],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 28),
                      ),
                      Text(
                        separaFecha(lista![posicion]['fechaHora']),
                        //lista![posicion]['fechaHora'],
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                      Text(
                        separaHora(lista![posicion]['fechaHora']),
                        //lista![posicion]['fechaHora'],
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                      SizedBox(height: 70.0),
                      Row(
                        children: [
                          FloatingActionButton(
                            heroTag: "subBtnEdit" + posicion.toString(),
                            child: Icon(Icons.edit),
                            onPressed: () {
                              print("Editar reserva");

                              index = posicion;
                              id = lista![posicion]['_id'];
                              showPopup(
                                  context,
                                  _popupBody(DetalleVigente(
                                    index: index,
                                    lista: lista,
                                  )),
                                  'Modificar/Anular reserva');
                              //consultaHistorial();
                              //Navigator.push(
                              //  context, MaterialPageRoute(builder: (context) => historial()));
                            },
                          ),
                          SizedBox(width: 20.0),
                          FloatingActionButton(
                            heroTag: "subBtnQR" + posicion.toString(),
                            child: Icon(Icons.schedule_send),
                            onPressed: () {
                              print("Atención");
                              showPopup(
                                  context,
                                  _popupBody(DetalleAtencion(
                                    index: index,
                                    lista: lista,
                                  )),
                                  'Atención');
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              )),
            ));
      },
    );
  }

  showPopup(BuildContext context, Widget widget, String title,
      {BuildContext? popupContext}) {
    Navigator.push(
      context,
      PopupLayout(
        top: 30,
        left: 30,
        right: 30,
        bottom: 50,
        bgColor: Colors.black.withOpacity(0.5),
        child: PopupContent(
          content: Scaffold(
            appBar: AppBar(
              title: Text(title),
              leading: new Builder(builder: (context) {
                return IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    try {
                      Navigator.pop(context);
                    } catch (e) {}
                  },
                );
              }),
              brightness: Brightness.light,
            ),
            resizeToAvoidBottomInset: false,
            body: widget,
          ),
        ),
      ),
    );
  }

  Widget _popupBody(Widget content) {
    return Container(
      // child: DetalleVigente(index: index, lista: lista),
      child: content,
    );
  }
}
