import 'package:flutter/material.dart';
//import 'package:wait/src/funcionalidades/home/Ajustes.dart';
import 'package:wait/src/funcionalidades/login/login.dart';
import 'package:wait/src/funcionalidades/fila/Fila.dart';
import 'package:wait/src/funcionalidades/configuraciones/ConfiguracionSocio.dart';

class Home_Socio extends StatefulWidget {
  final DatosUsuario? usuario;
  Home_Socio(this.usuario);
  @override
  _Home_SocioState createState() => _Home_SocioState();
}

class _Home_SocioState extends State<Home_Socio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
          child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 80,
            ),
            new Text('Bienvenido, ' + widget.usuario!.nombrePrincipal!),
            SizedBox(
              height: 80,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new ElevatedButton(
                  child: Text("Configuraciones"),
                  onPressed: () {
                    //TODO
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ConfiguracionSocio(widget.usuario!)));
                  },
                ),
                SizedBox(
                  width: 50,
                ),
                new ElevatedButton(
                  child: Text("Fila Virtual"),
                  onPressed: () {
                    //TODO
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Fila(widget.usuario)));
                  },
                ),
              ],
            )
          ],
        ),
      )),
    );
  }
}
