import 'dart:ui';

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  final estiloTexto = new TextStyle(fontSize: 30);

  int contador = 0;

  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bienvenido"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Numero de Clicks:', style: estiloTexto),
            Text(
                '$contador', // est '$contador' se llama interpolacion, es lo mismo que contador.toString()
                style: estiloTexto),
          ],
        ),
      ),
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.query_builder),
        onPressed: () {
          print("pase por aqui");
          contador = contador + 1;
          print("Nuevo valor de contador: " + contador.toString());
        },
      ),
    );
  }
}
