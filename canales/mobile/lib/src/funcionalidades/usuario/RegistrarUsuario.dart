import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../configuration/config.dart';
import 'package:flutter/services.dart';
import '../login/login.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class RegistrarUsuario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Registrar Usuario';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: SingleChildScrollView(
          child: RegisterForm(),
        ),
      ),
    );
  }
}

RespuestaCrearUsuario respuestaRecbida = new RespuestaCrearUsuario();

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _agreedToTOS = false;
  bool emailValid = false;
  bool phoneValid = false;
  DateTime birthDate = DateTime(1969, 1, 1);
  String? usuarioIngresado;
  String? passwordIngresado;
  int? rutIngresado;
  String? dvIngresado;
  String? emailIngresado;
  String? nombreIngresado;
  String? apellidoIngresado;
  String? telefonoIngresado;
  String? title;
  String? descr;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 16.0),
              _usuario(),
              const SizedBox(height: 16.0),
              _password(),
              const SizedBox(height: 16.0),
              _rut(),
              const SizedBox(height: 16.0),
              _email(),
              const SizedBox(height: 16.0),
              _nombre(),
              const SizedBox(height: 16.0),
              _apellido(),
              const SizedBox(height: 16.0),
              _telefono(),
              const SizedBox(height: 16.0),
              _calendario(),
              _tos(),
              Row(
                children: <Widget>[
                  const Spacer(),
                  ElevatedButton(
                    onPressed: _submittable() ? _submit : null,
                    child: const Text('Crear Usuario'),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  bool _submittable() {
    return _agreedToTOS;
  }

  void _submit() {
    if (_formKey.currentState!.validate()) {
      //creaUsuario();
      crearUsuario()
          .then((val) => setState(() {
                RespuestaCrearUsuario respuesta = val;

                if (respuesta.success == true) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(respuesta.message.toString())));
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (ctx) => Login()));
                } else {
                  title = respuesta.title.toString();
                  descr = respuesta.message.toString();
                  showAlertDialog(context);
                }
              }))
          .catchError((e) {
        print(e);
        title = 'Error';
        descr =
            'No es posible realizar la operación solicitada. Intente mas tarde.';
        showAlertDialog(context);
      });
    }
  }

  void _setAgreedToTOS(bool newValue) {
    setState(() {
      _agreedToTOS = newValue;
    });
  }

  void rebuildPage() {
    setState(() {});
  }

///////Alert Dialog
  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
    // set up the AlertDialog

    AlertDialog alert = AlertDialog(
      title: Text(title.toString()),
      content: Text(descr.toString()),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

/////////// Usuario
  Widget _usuario() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(50),
        FilteringTextInputFormatter.allow(RegExp(r"[/^\S*$/]")),
      ],
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        labelText: 'Usuario',
        prefixIcon: Icon(Icons.account_circle),
      ),
      validator: (String? value) {
        if (value != null) {
          if (value.trim().isEmpty) {
            return 'Usuario es requerido';
          } else {
            usuarioIngresado = value.trim();
            print(usuarioIngresado);
          }
        }
      },
    );
  }

/////////// Password
  Widget _password() {
    return TextFormField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        inputFormatters: [
          new LengthLimitingTextInputFormatter(20),
          FilteringTextInputFormatter.allow(RegExp(r"[/^\S*$/]")),
        ],
        obscureText: true,
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          labelText: 'Contraseña',
          prefixIcon: Icon(Icons.lock),
        ),
        validator: (String? value) {
          if (value != null) {
            if (value.trim().isEmpty) {
              return 'Contraseña es requerida';
            } else {
              if (value.length < 8) {
                return 'Contraseña debe contener al menos 8 caracteres';
              } else {
                passwordIngresado = value;
                print(passwordIngresado);
              }
            }
          }
        });
  }

//////// Rut
  Widget _rut() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              inputFormatters: [
                new LengthLimitingTextInputFormatter(8),
                FilteringTextInputFormatter.allow(RegExp("[0-9]")),
              ],
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Rut',
                prefixIcon: Icon(Icons.perm_identity_rounded),
              ),
              validator: (String? value) {
                if (value != null) {
                  if (value.trim().isEmpty) {
                    return 'Rut es requerido';
                  } else {
                    if (int.tryParse(value) == null) {
                      return 'Rut debe ser númerico';
                    } else {
                      rutIngresado = int.parse(value.trim());
                      print(rutIngresado);
                    }
                  }
                }
              },
            ),
          ),
          Expanded(
            child: TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              inputFormatters: [
                new LengthLimitingTextInputFormatter(1),
                FilteringTextInputFormatter.allow(RegExp("[0-9k-kK-K]")),
              ],
              decoration: InputDecoration(
                //border:
                //OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                labelText: 'DV',
                prefixIcon: Icon(Icons.horizontal_rule_rounded),
              ),
              validator: (String? value) {
                if (value != null) {
                  if (value.trim().isEmpty) {
                    return 'DV es requerido';
                  } else {
                    dvIngresado = value.trim();
                    print(dvIngresado);
                  }
                }
              },
            ),
          ),
        ],
      ),
    );
  }

//////// Widget Email
  Widget _email() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(50),
        FilteringTextInputFormatter.allow(RegExp(r"[/^\S*$/]")),
      ],
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        labelText: 'Email',
        hintText: 'contacto@wait.cl',
        prefixIcon: Icon(Icons.alternate_email_sharp),
      ),
      validator: (String? value) {
        if (value != null) {
          if (value.trim().isEmpty) {
            return 'Email es requerido';
          } else {
            emailValid = RegExp(
                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(value);
            if (!emailValid) {
              return 'Email ingresado no es válido';
            } else {
              emailIngresado = value.trim();
            }
          }
        }
      },
    );
  }

//////// Widget Nombre
  Widget _nombre() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(45),
        FilteringTextInputFormatter.allow(RegExp(r"[/^\S*$/]")),
      ],
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        labelText: 'Nombre',
        prefixIcon: Icon(Icons.contact_page_rounded),
      ),
      validator: (String? value) {
        if (value != null && value.trim().isEmpty) {
          return 'Nombre es requerido';
        } else {
          nombreIngresado = value;
          print(nombreIngresado);
        }
      },
    );
  }

//////// Widget Apellido
  Widget _apellido() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(45),
        FilteringTextInputFormatter.allow(RegExp(r"[/^\S*$/]")),
      ],
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        labelText: 'Apellido',
        prefixIcon: Icon(Icons.contact_page_rounded),
      ),
      validator: (String? value) {
        if (value != null && value.trim().isEmpty) {
          return 'Apellido es requerido';
        } else {
          apellidoIngresado = value;
          print(apellidoIngresado);
        }
      },
    );
  }

//////// Widget Teléfono
  Widget _telefono() {
    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      inputFormatters: [
        new LengthLimitingTextInputFormatter(12),
        FilteringTextInputFormatter.allow(RegExp("[0-9+]")),
      ],
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        labelText: 'Teléfono',
        hintText: '+56911111111',
        prefixIcon: Icon(Icons.phone),
      ),
      validator: (String? value) {
        if (value != null) {
          if (value.trim().isEmpty) {
            return 'Teléfono es requerido';
          } else {
            phoneValid = RegExp(r'(^\+56[2-9]{1}[0-9]{8}$)').hasMatch(value);
            if (!phoneValid) {
              return 'Teléfono ingresado no es válido';
            } else {
              telefonoIngresado = value;
              print(telefonoIngresado);
            }
          }
        }
      },
    );
  }

//////// Widget Calendario
  Widget _calendario() {
    return Container(
        height: 80,
        child: InputDecorator(
          decoration: InputDecoration(
            labelText: 'Fecha de Nacimiento',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: birthDate,
            onDateTimeChanged: (DateTime newDateTime) {
              birthDate = newDateTime;
              print({"fecha de nacimiento ", birthDate});
            },
          ),
        ));
  }

  //////// Widget TOS
  Widget _tos() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        children: <Widget>[
          Checkbox(
            value: _agreedToTOS,
            onChanged: (bool? _setAgreedToTOS) async {
              if (_setAgreedToTOS != null) {
                return;
              }
            },
          ),
          GestureDetector(
            onTap: () => _setAgreedToTOS(!_agreedToTOS),
            child: const Text(
              'Estoy de acuerdo con los términos y condiciones.',
            ),
          ),
        ],
      ),
    );
  }

  //Future<bool> creaUsuario() async {
  Future<RespuestaCrearUsuario> crearUsuario() async {
    var uri = nodeHost + nodePort + rutaCrearUsuario;
    print("URI POST: " + uri);

    var body = json.encode({
      "usuario": usuarioIngresado.toString(),
      "password": passwordIngresado.toString(),
      "rut": rutIngresado,
      "dv": dvIngresado.toString(),
      "nombre_principal": nombreIngresado.toString(),
      "nombre_secundario": "",
      "apellido_principal": apellidoIngresado.toString(),
      "apellido_secundario": "",
      "fecha_nacimiento": birthDate.toString(),
      "direccion": "",
      "correo": emailIngresado.toString(),
      "numero_contacto": telefonoIngresado.toString(),
    });

    try {
      final http.Response response = await http.post(Uri.parse(uri),
          headers: {
            'Content-type': 'application/json',
            'Accept': 'application/json',
          },
          body: body);
      respuestaRecbida =
          RespuestaCrearUsuario.fromJson(jsonDecode(response.body));
      print(respuestaRecbida);
      return respuestaRecbida;
    } catch (e) {
      print(e);
      throw Exception('Error de Comunicación con API REST');
    }
  }
}

class RespuestaCrearUsuario {
  final String? status;
  final bool? success;
  final String? title;
  final String? message;

  RespuestaCrearUsuario({this.status, this.success, this.title, this.message});

  factory RespuestaCrearUsuario.fromJson(Map<String, dynamic> json) {
    return RespuestaCrearUsuario(
      status: json['status'],
      success: json['success'],
      title: json['title'],
      message: json['message'],
    );
  }
}
