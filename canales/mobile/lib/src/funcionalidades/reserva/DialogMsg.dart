import 'package:flutter/material.dart';
import 'package:wait/src/funcionalidades/home/Home_Usuario.dart';
import 'package:wait/src/funcionalidades/home/home_page.dart';
import 'package:wait/src/funcionalidades/login/login.dart';

class DialogMsg {
  //DialogMsg _instance = new DialogMsg.internal();
  DatosUsuario? usuario;
  DialogMsg(this.usuario);
  DialogMsg.internal();

  //factory DialogMsg() => _instance;

  void showCustomDialog(
    BuildContext context, {
    required String titulo,
    String okBtnText = "Ok",
    String cancelBtnText = "Cancelar",
    required bool estado,
    required String mensaje,
  }) {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(titulo),
            content: Text(mensaje),
            actions: <Widget>[
              TextButton(
                child: Text(okBtnText),
                onPressed: () {
                  if (estado == 0) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Home_Usuario(usuario)));
                  } else {
                    Navigator.pop(context);
                  }
                  //Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}
