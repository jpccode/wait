import 'package:flutter/material.dart';
import 'package:wait/src/funcionalidades/login/login.dart';

//import 'package:http/http.dart' as http;

//import 'EditarReserva.dart';

class DetalleHistorial extends StatefulWidget {
  final int? index;
  final List? lista;
  final DatosUsuario? usuario;

  DetalleHistorial({this.index, this.lista, this.usuario});

  @override
  _DetalleHistorialState createState() => _DetalleHistorialState();
}

class _DetalleHistorialState extends State<DetalleHistorial> {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Column(
        children: <Widget>[
          new Text(
            widget.lista![widget.index!]['nombreSocio'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          new Text(
            widget.lista![widget.index!]['nombreSucursal'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          new Text(
            widget.lista![widget.index!]['direccion'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          new Text(
            widget.lista![widget.index!]['fechaHora'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
          new Text(
            widget.lista![widget.index!]['nombreEstado'].toString(),
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          ),
        ],
      ),
    );
  }
}
