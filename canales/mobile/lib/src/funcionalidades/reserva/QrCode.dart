import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:convert';

class QrCode extends StatefulWidget {
  final int? index;
  final List? lista;
  QrCode({this.index, this.lista});
  @override
  _QrCodeState createState() => _QrCodeState();
}

String formatoCodigo(List reg, int index) {
  Map<String, dynamic> obj = {
    'fechaReserva': reg[index]['fechaHora'],
    'codigoValidacion': reg[index]['_id'],
    'status': "Reserva válida. 2021 - Wait."
  };
  String codigoQR = jsonEncode(obj);
  return codigoQR;
}

class _QrCodeState extends State<QrCode> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: QrImage(
          data: formatoCodigo(widget.lista!, widget.index!),
          version: QrVersions.auto,
          size: 320,
        ),
      ),
    );
  }
}
