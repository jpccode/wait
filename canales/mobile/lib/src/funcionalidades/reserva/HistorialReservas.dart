import 'package:flutter/material.dart';
import 'package:wait/src/funcionalidades/login/login.dart';
import 'package:wait/src/funcionalidades/reserva/DetalleHistorial.dart';
import 'package:wait/src/funcionalidades/reserva/PopupContent.dart';
import 'package:wait/src/funcionalidades/reserva/PopupView.dart';
import 'package:http/http.dart' as http;
import '../configuration/config.dart';

import 'dart:convert';
import 'dart:async';

class HistorialReservas extends StatefulWidget {
  final DatosUsuario? usuario;
  HistorialReservas(this.usuario);
  @override
  _HistorialReservasState createState() => _HistorialReservasState();
}

class _HistorialReservasState extends State<HistorialReservas> {
  Future<List> obtenerReservas(String action) async {
    final int idUser = widget.usuario!.id!;
    try {
      final response = await http.get(Uri.parse(
          nodeHost+nodePort+rutaReserva +
              idUser.toString() +
              '?action=' +
              action));
      return json.decode(response.body);
    } catch (e) {
      print('Exception = ' + e.toString());
      throw Exception(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Text("Historial de Reservas"),
      ),
      body: new FutureBuilder<List>(
        future: obtenerReservas('historial'),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? new ElementoLista(
                  lista: snapshot.data,
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                );
        },
      ),
/*       floatingActionButton: new FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new CrearReserva(),
        )),
      ), */
    );
  }
}

class ElementoLista extends StatelessWidget {
  final List? lista;
  int? index = 0;
  final DatosUsuario? usuario;
  ElementoLista({this.lista, this.usuario});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ListView.builder(
      itemCount: lista == null ? 0 : lista!.length,
      itemBuilder: (context, posicion) {
        return new Container(
          padding: EdgeInsets.all(2.0),
          child: new GestureDetector(
            onTap: () {
              index = posicion;
/*               Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new DetalleReserva(
                  index: posicion,
                  lista: lista,
                ),
              )); */
              showPopup(BuildContext context, Widget widget, String title,
                  {BuildContext? popupContext}) {
                Navigator.push(
                  context,
                  PopupLayout(
                    top: 30,
                    left: 30,
                    right: 30,
                    bottom: 50,
                    bgColor: Colors.black.withOpacity(0.5),
                    child: PopupContent(
                      content: Scaffold(
                        appBar: AppBar(
                          title: Text(title),
                          leading: new Builder(builder: (context) {
                            return IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () {
                                try {
                                  Navigator.pop(context); //close the popup
                                } catch (e) {}
                              },
                            );
                          }),
                          brightness: Brightness.light,
                        ),
                        resizeToAvoidBottomInset: false,
                        body: widget,
                      ),
                    ),
                  ),
                );
              }

              showPopup(context, _popupBody(), 'Detalle de Historial');
            },
            child: new Card(
              color: Colors.deepPurple,
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  lista![posicion]['nombreSocio'] +
                      ": " +
                      lista![posicion]['fechaHora'],
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _popupBody() {
    return Container(
      child: DetalleHistorial(index: index, lista: lista, usuario: usuario),
    );
  }
}
