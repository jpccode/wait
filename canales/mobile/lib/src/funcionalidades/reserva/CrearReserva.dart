//import 'dart:convert';

import 'dart:io';

import 'package:flutter/material.dart';
import '../configuration/config.dart';
//import 'package:date_format/date_format.dart';
import 'package:http/http.dart' as http;
import 'package:wait/src/funcionalidades/login/login.dart';
import 'dart:convert';
import 'package:flutter/services.dart';

import 'DialogMsg.dart';

class CrearReserva extends StatefulWidget {
  DatosUsuario? usuario;
  CrearReserva(this.usuario);
  @override
  _CrearReservaState createState() => _CrearReservaState();
}

class _CrearReservaState extends State<CrearReserva> {
  @override
  void initState() {
    cargaSocios();
    // _cargaDireccion();
    super.initState();
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(), // Refer step 1
      firstDate: DateTime.now(),
      lastDate: DateTime(2022),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: firstTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Text("Nueva Reserva"),
      ),
      body: new Container(
        padding: EdgeInsets.all(10.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              _idUsuario(),
              DropdownButtonFormField(
                  //dropdown socios
                  items: dataSocio.map((item) {
                    return DropdownMenuItem(
                      child: Text(item["NOMBRE_SOCIO"]),
                      value: item["ID"],
                    );
                  }).toList(),
                  hint: Text('Seleccione el lugar'),
                  onChanged: (newVal) {
                    setState(() {
                      selectedSocio = int.parse(newVal.toString());
                      selectedSucursal = null;
                      cargaSucursales();
                      _cargaDireccion();
                    });
                  },
                  value: selectedSocio,
                  validator: (selectedSocio) {
                    if (selectedSocio == null) {
                      return 'Debe seleccionar un lugar';
                    }
                  }),
              DropdownButton(
                //dropdown sucursales
                items: dataSucursal.map((item) {
                  return DropdownMenuItem(
                    child: Text(item["NOMBRE_SUCURSAL"]),
                    value: item["ID"],
                  );
                }).toList(),
                hint: Text('Seleccione sucursal'),
                onChanged: (newVal) {
                  setState(() {
                    selectedSucursal = int.parse(newVal.toString());
                    pkConfig = selectedSocio.toString() +
                        "." +
                        selectedSucursal.toString();
                    _cargaDireccion();
                    print(
                        {"Socio", selectedSocio, "Sucursal", selectedSucursal});
                  });
                },
                value: selectedSucursal,
              ),
              Visibility(
                child: SizedBox(
                  child: Column(
                    children: [
                      Text(
                        nombreSoc,
                        style: TextStyle(
                            fontSize: 22,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        nombreSuc,
                        style: TextStyle(fontSize: 22, color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        direccion,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        telefono,
                        style: TextStyle(fontSize: 16, color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        correo,
                        style: TextStyle(fontSize: 16, color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
              new ElevatedButton(
                onPressed: () => _selectDate(context), // Refer step 3
                child: Text(
                  'Seleccione la fecha',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              new ElevatedButton(
                onPressed: () => _selectTime(context), // Refer step 3
                child: Text(
                  'Ingrese la hora...',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              new ElevatedButton(
                child: new Text("Reservar"),
                onPressed: () async {
                  var valid = await validaReserva();
                  int codigo = valid['cod'];
                  String titulo = valid['titulo'];
                  String mensaje = valid['msg'];
                  bool estado;
                  if (codigo == 0) {
                    estado = true;
                  } else {
                    estado = false;
                  }

                  DialogMsg(widget.usuario!).showCustomDialog(
                    context,
                    titulo: titulo,
                    estado: estado,
                    mensaje: mensaje,
                  );
                  //if (selectedSocio != null) {
                  // cargaConfig();

                  // }

                  // DialogMsg();
                  //creaReserva();
                  //Navigator.pop(context, 'Reserva generada exitosamente.');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  TextEditingController controlSocio = new TextEditingController();
  TextEditingController controlEmail = new TextEditingController();
  // DateTime selectedDate = DateTime.now();
  DateTime selectedDate = DateTime.now();
  TimeOfDay firstTime = TimeOfDay(hour: 08, minute: 00);
  TimeOfDay selectedTime = TimeOfDay(hour: 08, minute: 00);
  int? idUser;
  int? usuarioIngresado;
  String? nombreUsuario;
  String? apellidoUsuario;
  String? pkConfig;
  int? selectedSocio, selectedSucursal;
  List dataSocio = [];
  List dataSucursal = [];
  List dataConfig = [];
  Map<String, Object> configuracion = {};
  Map<String, dynamic>? datosDireccion;
  String direccion = '',
      nombreSoc = '',
      nombreSuc = '',
      telefono = '',
      correo = '';

  Future<String> cargaSocios() async {
    var obtenerSocio;
    if (widget.usuario!.tipoUsuario! == 'USUARIO') {
      obtenerSocio = rutaSocios;
    } else {
      if (widget.usuario!.tipoUsuario! == 'EJECUTIVO') {
        obtenerSocio = rutaSocio + widget.usuario!.idSocio!.toString();
      }
    }
    final String uri = nodeHost + nodePort + obtenerSocio;
    var response = await http.get(Uri.parse(uri));

    if (response.statusCode == 200) {
      var res = await http
          .get(Uri.parse(uri), headers: {"Accept": "application/json"});

      var resBody = json.decode(res.body.toString());
      List temp = resBody;

      setState(() {
        dataSocio = temp[0];
      });

      print('Loaded Successfully');
      return "Loaded Successfully";
    } else {
      throw Exception('Failed to load data.');
    }
  }

  Future<String> _cargaDireccion() async {
    String llave = '';
    String ruta = '';
    if (selectedSocio == null) {
      llave = "0.0";
    }
    if (selectedSucursal == 0 || selectedSucursal == null) {
      ruta = "infosocio/";
    } else {
      ruta = "infosucursal/";
    }
    if (selectedSucursal == null) {
      llave = selectedSocio.toString() + ".0";
    } else {
      llave = selectedSocio.toString() + "." + selectedSucursal.toString();
    }

    final String uri = nodeHost + nodePort + rutaHome + ruta + llave;
    var response = await http.get(Uri.parse(uri));
    if (response.statusCode == 200) {
      var res = await http
          .get(Uri.parse(uri), headers: {"Accept": "application/json"});

      var resBody = json.decode(res.body.toString());
      List temp = resBody;
      List temp2 = temp[0];
      Map<String, dynamic>? item = {};
      if (temp2.length != 0) {
        item = temp2[0];
        datosDireccion = item;
      }
      print(llave);
      print(uri);
      setState(() {
        //dataSocio = temp[0];
        datosDireccion = item;
        nombreSoc = datosDireccion!['NOMBRE_SOCIO'];
        nombreSuc = datosDireccion!['NOMBRE_SUCURSAL'];
        direccion = datosDireccion!['DIRECCION'];
        telefono = datosDireccion!['NUMERO_CONTACTO'];
        correo = datosDireccion!['CORREO_CONTACTO'];
      });

      print('Loaded Successfully');
      return "Loaded Successfully";
    } else {
      throw Exception('Failed to load data.');
    }
  }

  Future<String> cargaSucursales() async {
    final String uri = nodeHost +
        nodePort +
        rutaSucursales +
        selectedSocio!.toString() +
        "?action=all";

    var response = await http.get(Uri.parse(uri));

    if (response.statusCode == 200) {
      var res = await http
          .get(Uri.parse(uri), headers: {"Accept": "application/json"});

      var resBody = json.decode(res.body.toString());
      List temp = resBody;

      setState(() {
        selectedSucursal = null;
        // dataSucursal.map((e) => null)
        dataSucursal = temp[0];
      });

      print('Loaded Successfully');

      return "Loaded Successfully";
    } else {
      throw Exception('Failed to load data.');
    }
  }

  Future<String> cargaConfig(llave) async {
    final String uri = nodeHost + nodePort + rutaConfig + llave;
    var response = await http.get(Uri.parse(uri));

    if (response.statusCode == 200) {
      var res = await http
          .get(Uri.parse(uri), headers: {"Accept": "application/json"});
      var resBody = json.decode(res.body.toString());
      var temp = resBody;
      var temp2 = temp[0];
      var item = temp2[0];

      configuracion = {
        'aforo': item['AFORO'],
        'dotacion': item['DOTACION_PERSONAL'],
        'tiempoAtraso': item['TIEMPO_ATRASO'],
        'config': item['CONFIG_HORARIO'].toString()
      };
      print('Loaded Successfully');

      return "Loaded Successfully";
    } else {
      throw Exception('Failed to load data.');
    }
  }

  /////////// Usuario
  Widget _idUsuario() {
    bool esEjecutivo = false;
    if (widget.usuario!.tipoUsuario! == 'EJECUTIVO') {
      esEjecutivo = true;
    }
    return Visibility(
        visible: esEjecutivo,
        child: TextFormField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          inputFormatters: [
            new LengthLimitingTextInputFormatter(50),
            FilteringTextInputFormatter.allow(RegExp(r"[/^\S*$/]")),
          ],
          decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
            labelText: 'ID Usuario',
            prefixIcon: Icon(Icons.account_circle),
          ),
          validator: (String? value) {
            if (value != null) {
              if (value.trim().isEmpty) {
                return 'ID Usuario es requerido';
              } else {
                usuarioIngresado = int.parse(value.trim());
                print(usuarioIngresado);
              }
            }
          },
        ));
  }

  Future validaReserva() async {
    Map<String, dynamic> response = {'titulo': '', 'cod': 0, 'msg': ''};
//validar que se haya elegido un socio
    if (selectedSocio != null) {
      if (selectedSucursal == null) {
        //si no escogió o no existe sucursal, se utiliza  la casa matriz
        selectedSucursal = 0;
      }
      var llave = selectedSocio.toString() + "." + selectedSucursal.toString();
      await cargaConfig(llave);
      String horario = configuracion['config'].toString();

      //si el año es distinto de cero, han elegido fecha
      if (selectedDate.year > 0) {
        Map<String, dynamic> mapa = jsonDecode(horario);
        List<dynamic> dias = mapa['dias'];
        int diasemana = selectedDate.weekday;
        for (var i = 0; i < dias.length; i++) {
          Map<String, dynamic> item = dias[i];
          if (item['numero'] == diasemana) {
            //el día de la reserva debe estar configurado como día hábil
            if (dias[i]['diahabil'] == true) {
              DateTime horaSelec = DateTime(
                  selectedDate.year,
                  selectedDate.month,
                  selectedDate.day,
                  selectedTime.hour,
                  selectedTime.minute);

              String cadenaHora = item['horainicio'];
              int hr = int.parse(cadenaHora.split(':')[0]);
              int min = int.parse(cadenaHora.split(':')[1]);

              DateTime horaini = DateTime(selectedDate.year, selectedDate.month,
                  selectedDate.day, hr, min);
              cadenaHora = item['horatermino'];
              hr = int.parse(cadenaHora.split(':')[0]);
              min = int.parse(cadenaHora.split(':')[1]);
              DateTime horafin = DateTime(selectedDate.year, selectedDate.month,
                  selectedDate.day, hr, min);
              //la hora debe estar dentro del horario de atención del socio/sucursal
              if (!(horaSelec.isBefore(horaini)) &&
                  !(horaSelec.isAfter(horafin))) {
                creaReserva();
                response['titulo'] = 'Confirmación de Reserva';
                response['cod'] = 0;
                response['msg'] =
                    // 'Se dispone a agendar la reserva. ¿Desea continuar?';
                    'Reserva generada exitosamente';
              } else {
                response['titulo'] = 'Algo salió mal :(';
                response['cod'] = 4;
                response['msg'] = 'Horario fuera de rango (' +
                    dias[i]['horainicio'].toString() +
                    "-" +
                    dias[i]['horatermino'] +
                    ")";
              }
            } else {
              response['titulo'] = 'Algo salió mal :(';
              response['cod'] = 3;
              response['msg'] = 'Ha seleccionado un día no laboral';
            }
          }
        }
      } else {
        response['titulo'] = 'Algo salió mal :(';
        response['cod'] = 2;
        response['msg'] = 'Debe seleccionar una fecha';
      }
    } else {
      response['titulo'] = 'Algo salió mal :(';
      response['cod'] = 1;
      response['msg'] = 'Debe seleccionar un lugar';
    }
    return response;
  }

  void creaReserva() async {
    if (usuarioIngresado != null) {
      idUser = usuarioIngresado;
      nombreUsuario = 'Reserva Ejecutivo';
      apellidoUsuario = '';
    } else {
      idUser = widget.usuario!.id!;
      nombreUsuario = widget.usuario!.nombrePrincipal;
      apellidoUsuario = widget.usuario!.apellidoPrincipal!;
    }
    var url = nodeHost + nodePort + rutaReservas;
    var fechaNew = DateTime(selectedDate.year, selectedDate.month,
        selectedDate.day, selectedTime.hour, selectedTime.minute);
    var body = json.encode({
      "idUsuario": idUser,
      "nombreUsuario": nombreUsuario,
      "apellidoUsuario": apellidoUsuario,
      "idSocio": selectedSocio,
      "nombreSocio": nombreSoc,
      "idSucursal": selectedSucursal,
      "nombreSucursal": nombreSuc,
      "direccion": direccion,
      "idEstado": 1,
      "nombreEstado": "Vigente",
      "fechaHora": fechaNew.toString(),
      "fechaCreacion": DateTime.now().toString(),
    });
    await http.post(Uri.parse(url),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
        },
        body: body);
  }
}
