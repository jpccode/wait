//import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../configuration/config.dart';

import 'package:intl/intl.dart';
import 'package:wait/src/funcionalidades/login/login.dart';
import 'package:wait/src/funcionalidades/fila/fila.dart';

class DetalleVigente extends StatefulWidget {
  final int? index;
  final DatosUsuario? usuario;
  final List? lista;
  DetalleVigente({this.index, this.lista, this.usuario});

  @override
  _DetalleVigenteState createState() => _DetalleVigenteState();
}

class _DetalleVigenteState extends State<DetalleVigente> {
  @override
  void initState() {
    super.initState();
  }

  _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime.now(),
      lastDate: DateTime(2022),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
      });
  }

  String separaFecha(String date) {
    var formatoFecha = DateFormat("dd-MM-yyyy");
    DateTime temp = DateTime.parse(date);
    var fecha = formatoFecha.format(temp);
    return fecha;
  }

  String separaHora(String date) {
    var formatoHora = DateFormat("HH:mm:ss");
    DateTime temp = DateTime.parse(date);
    var hora = formatoHora.format(temp);
    return hora;
  }

  bool isSwitched = false;
  bool habilitaBotones = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: new Column(
        children: <Widget>[
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Fecha de la reserva: ' +
                    separaFecha(
                        widget.lista![widget.index!]['fechaHora'].toString()),
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(width: 20.0),
              new Visibility(
                visible: habilitaBotones,
                child: new FloatingActionButton(
                  onPressed: () => _selectDate(context), // Refer step 3
                  child: Icon(
                    Icons.edit,
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Hora de la reserva: ' +
                    separaHora(
                        widget.lista![widget.index!]['fechaHora'].toString()),
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(width: 20.0),
              new Visibility(
                visible: habilitaBotones,
                child: new FloatingActionButton(
                    onPressed: () => _selectTime(context), // Refer step 3
                    child: Icon(
                      Icons.edit,
                    )),
              )
            ],
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Deseo anular mi reserva',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(width: 10.0),
              Switch(
                value: isSwitched,
                onChanged: (value) {
                  setState(() {
                    isSwitched = value;
                    habilitaBotones = !value;
                    print(isSwitched);
                  });
                },
                activeTrackColor: Colors.yellow,
                activeColor: Colors.orangeAccent,
              ),
            ],
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new ElevatedButton(
                child: new Text("Guardar"),
                onPressed: () {
                  newid = widget.lista![widget.index!]['_id'].toString();
                  actualizaReserva();
                  Navigator.pop(context, 'Reserva actualizada exitosamente.');
                },
              )
            ],
          ),
          //new Visibility(visible: true, child: new Text(newid!)),
        ],
      ),
    );
  }

  String? newid;
  final TextEditingController controlFecha = new TextEditingController();
  final TextEditingController controlHora = new TextEditingController();
  // DateTime selectedDate = DateTime.now();
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay(hour: 08, minute: 00);
  int? idUser = 1, idEstado = 1;
  late DateTime fechaNew;
  void actualizaReserva() async {
    var url = nodeHost + nodePort + rutaReserva + newid!;
    String estadoNew = 'Vigente';
    String motivoAnular = 'Anulada por el usuario';

    if (isSwitched == true) {
      //se anula reserva
      idEstado = 4;
      estadoNew = motivoAnular;
      fechaNew = selectedDate;
    } else {
      //se modifica fechaHora
      idEstado = 1;
      estadoNew = 'Vigente';
      fechaNew = DateTime(selectedDate.year, selectedDate.month,
          selectedDate.day, selectedTime.hour, selectedTime.minute);
    }
    var body = json.encode({
      "idEstado": idEstado,
      "nombreEstado": estadoNew,
      "fechaHora": fechaNew.toString(),
    });
    await http.put(Uri.parse(url),
        headers: {
          'Content-type': 'application/json',
          'Accept': 'application/json',
        },
        body: body);
  }
}
