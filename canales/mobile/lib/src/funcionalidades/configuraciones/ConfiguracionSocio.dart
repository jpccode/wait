import 'package:flutter/material.dart';
import 'package:wait/src/funcionalidades/login/login.dart';
import 'package:http/http.dart' as http;
import '../configuration/config.dart';

import 'dart:convert';
import 'dart:async';

class ConfiguracionSocio extends StatefulWidget {
  final DatosUsuario? usuario;
  ConfiguracionSocio(this.usuario);
  @override
  _ConfiguracionSocioState createState() => _ConfiguracionSocioState();
}

class _ConfiguracionSocioState extends State<ConfiguracionSocio> {
  Map<String, Object> configuracion = {};
  final _aforoCtrl = TextEditingController();
  final _dotacionCtrl = TextEditingController();
  final _atrasoCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
/*     cargarConfig();
    _aforoCtrl.text = configuracion['aforo'].toString(); */
    _cargarDatosConfig();
  }

  _cargarDatosConfig() async {
    await cargarConfig();
    _aforoCtrl.text = configuracion['aforo'].toString();
    _dotacionCtrl.text = configuracion['dotacion'].toString();
    _atrasoCtrl.text = configuracion['tiempoAtraso'].toString();
  }

  Future<Map<String, Object>> cargarConfig() async {
    // String llave = widget.usuario!.id.toString();
    String llave = '8.0';
    try {
      final response =
          await http.get(Uri.parse(nodeHost + nodePort + rutaConfig + llave));
      var resBody = json.decode(response.body.toString());
      var temp = resBody;
      var temp2 = temp[0];
      var item = temp2[0];
      configuracion = {
        'aforo': item['AFORO'],
        'dotacion': item['DOTACION_PERSONAL'],
        'tiempoAtraso': item['TIEMPO_ATRASO'],
      };
      return configuracion;
    } catch (e) {
      print('Exception = ' + e.toString());
      throw Exception(e);
    }
  }

  Future<List<dynamic>> cargarHorario() async {
    // String llave = widget.usuario!.id.toString();
    String llave = '8.0';
    try {
      final response =
          await http.get(Uri.parse(nodeHost + nodePort + rutaConfig + llave));
      var resBody = json.decode(response.body.toString());
      var temp = resBody;
      var temp2 = temp[0];
      var item = temp2[0];
      var horario;
      var jsonTemp = jsonDecode(item['CONFIG_HORARIO']);
      horario = jsonTemp['dias'];

      return horario;
    } catch (e) {
      print('Exception = ' + e.toString());
      throw Exception(e);
    }
  }

  void updateConfig() async {}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        appBar: AppBar(
          title: Text("Configuraciones"),
        ),
        body: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  Text('Aforo Presencial: '),
                  SizedBox(
                    height: 30,
                    width: 80,
                    child: TextField(
                      controller: _aforoCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(border: OutlineInputBorder()),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text('Dotación Personal: '),
                  SizedBox(
                    height: 30,
                    width: 80,
                    child: TextField(
                      controller: _dotacionCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(border: OutlineInputBorder()),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text('Tiempo permitido de atraso: '),
                  SizedBox(
                    height: 30,
                    width: 80,
                    child: TextField(
                      controller: _atrasoCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(border: OutlineInputBorder()),
                      onChanged: (newVal) {
                        _atrasoCtrl.text = newVal.toString();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                'Horarios de Atención',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Flexible(
                  child: new FutureBuilder<List>(
                future: cargarHorario(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) print(snapshot.error);
                  return snapshot.hasData
                      ? new ElementoLista(
                          lista: snapshot.data,
                        )
                      : new Center(
                          child: new CircularProgressIndicator(),
                        );
                },
              )),
              Center(
                child: ElevatedButton(
                  child: Text('Guardar'),
                  onPressed: () {},
                ),
              )
            ],
          ),
        ));
  }
}

class ElementoLista extends StatefulWidget {
  final List? lista;
  int? index = 0;
  final DatosUsuario? usuario;
  ElementoLista({this.lista, this.usuario});

  @override
  _ElementoListaState createState() => _ElementoListaState();
}

class _ElementoListaState extends State<ElementoLista> {
  List<dynamic> switched = [];
  List<dynamic> horaIniTxt = [];
  List<dynamic> horaFinTxt = [];
  List<dynamic> horaIniTime = [];
  List<dynamic> horaFinTime = [];
  List<dynamic> pickers = [];
  TimeOfDay selectedTime = TimeOfDay(hour: 08, minute: 00);
  TimeOfDay firstTime = TimeOfDay(hour: 08, minute: 00);
  List<dynamic> txtCtrl = [];
  var _horaIniCtrl = new TextEditingController();

  @override
  void initState() {
    super.initState();
    /* firstTime =
        TimeOfDay(hour: int.parse(widget.lista![0]['diahabil']), minute: 00); */
    for (var i = 0; i < widget.lista!.length; i++) {
      //listaBool.add(widget.lista![i]['diahabil']);
      switched.add(widget.lista![i]['diahabil']);

      var cadenaTime = widget.lista![i]['horainicio'].toString().split(':');
      int hora = int.parse(cadenaTime[0]);
      int min = int.parse(cadenaTime[1]);
      horaIniTime.add(TimeOfDay(hour: hora, minute: min));
      horaIniTxt.add(widget.lista![i]['horainicio'].toString());

      cadenaTime = widget.lista![i]['horatermino'].toString().split(':');
      hora = int.parse(cadenaTime[0]);
      min = int.parse(cadenaTime[1]);
      horaFinTime.add(TimeOfDay(hour: hora, minute: min));
      horaFinTxt.add(widget.lista![i]['horatermino'].toString());
      //pickers.add(_selectTime(context, horaIniTime[i], horaIniTime[i]));
      txtCtrl.add(new TextEditingController());
      TextEditingController ctrl = txtCtrl[i];
      ctrl.text = horaIniTxt[i];
    }
  }

  Future<Null> _selectTime(BuildContext context, TimeOfDay hora) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: hora,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ListView.builder(
      itemCount: widget.lista == null ? 0 : widget.lista!.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, posicion) {
        return new Container(
          padding: EdgeInsets.all(2.0),
          child: new GestureDetector(
            onTap: () {},
            child: new Card(
              color: Colors.deepPurple.shade100,
              child: new Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Text(
                      widget.lista![posicion]['nombre'],
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                    Row(
                      children: [
                        Text(
                          'Día Hábil: ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Switch(
                          value: switched[posicion],
                          onChanged: (newVal) {
                            setState(() {
                              switched[posicion] = newVal;
                            });
                          },
                          activeTrackColor: Colors.yellow,
                          activeColor: Colors.orangeAccent,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'Hora inicio',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        SizedBox(
                          width: 80,
                          child: Text(
                            horaIniTxt[posicion],
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        SizedBox(width: 30),
                        Visibility(
                          visible: switched[posicion],
                          child: IconButton(
                            icon: Icon(Icons.timer),
                            iconSize: 30,
                            color: Colors.brown,
                            tooltip: 'Cambiar la hora de inicio de jornada',
                            onPressed: () {
                              //pickers[posicion];
                              setState(() {
                                _selectTime(context, horaIniTime[posicion]);
                              });
                            },
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          'Hora término',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        SizedBox(
                          width: 80,
                          child: Text(
                            horaFinTxt[posicion],
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Visibility(
                          visible: switched[posicion],
                          child: IconButton(
                            icon: Icon(Icons.timer),
                            iconSize: 30,
                            color: Colors.brown,
                            tooltip: 'Cambiar la hora de término de jornada',
                            onPressed: () {
                              //pickers[posicion];
                              setState(() {
                                _selectTime(context, horaFinTime[posicion]);
                              });
                            },
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class Horario {
  bool? habil;
  String? horaini, horafin;
  Horario({this.habil, this.horaini, this.horafin});
  factory Horario.fromJson(Map<String, dynamic> json) {
    return Horario(
        habil: json['diahabil'],
        horaini: json['horainicio'],
        horafin: json['horatermino']);
  }
}

class Config {
  int? aforo, dotacion, atraso;
  Horario? hor;
  Config({this.aforo, this.dotacion, this.atraso, this.hor});
  factory Config.fromJson(Map<String, dynamic> json) {
    return Config(
        aforo: json['AFORO'],
        dotacion: json['DOTACION_PERSONAL'],
        atraso: json['TIEMPO_ATRASO'],
        hor: json['diahabil']);
  }
}
