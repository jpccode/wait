import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:wait/src/funcionalidades/login/login.dart';

class Wait extends StatelessWidget {
  @override
  Widget build(context) {
    return MaterialApp(
        //supportedLocales: [loc],
        //locale: const Locale('es', ''),
        localizationsDelegates: GlobalMaterialLocalizations.delegates,
        supportedLocales: [
          const Locale('es', 'CL'),
          // const Locale('en', 'US'),
        ],
        debugShowCheckedModeBanner: false,
        home: Center(/* child: HomePageStateFull() */ child: Login()));
  }
}
