#!/bin/bash
echo "----------------------------------------------"
echo "               WAIT"
echo "----------------------------------------------"
echo "Script inicial de Base de datos"

#Carga de variables
#source ../config/variables.sh

. ../bbdd/config/variables.sh
#Despliegue
#Gestion de tablas y datos
##Mysql
mysql --host=127.0.0.1 --port=3306 -u root -D $MYSQL_DATABASE -p < ../bbdd/data/WAIT-MODEL-DB.sql
mysql --host=127.0.0.1 --port=3306 -u root -D $MYSQL_DATABASE -p < ../bbdd/data/stored_procedures.sql