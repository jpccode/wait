-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.38-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para wait
CREATE DATABASE IF NOT EXISTS `wait` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wait`;

-- Volcando estructura para procedimiento wait.sp_cargarInfoSocios
DELIMITER //
CREATE PROCEDURE `sp_cargarInfoSocios`()
BEGIN
SELECT S.ID, S.NOMBRE_SOCIO, S.DIRECCION, S.NUMERO_CONTACTO FROM socio S;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_cargarInfoSucursales
DELIMITER //
CREATE PROCEDURE `sp_cargarInfoSucursales`(
	IN `idSocio` INT
)
BEGIN
SELECT S.ID, S.NOMBRE_SUCURSAL, S.DIRECCION, S.NUMERO_CONTACTO FROM sucursal S WHERE S.ID_SOCIO=idSocio;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_delete_socio
DELIMITER //
CREATE PROCEDURE `sp_delete_socio`(
	IN `idSocio` INT
)
BEGIN
DELETE FROM socio WHERE ID = idSocio;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getSocio
DELIMITER //
CREATE PROCEDURE `sp_getSocio`(
	IN `idSocio` INT
)
BEGIN
SELECT * FROM socio WHERE ID=idSocio;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_insertUsuario
DELIMITER //
CREATE PROCEDURE `sp_insertUsuario`(
	IN `usuarioNew` VARCHAR(50),
	IN `passwordNew` VARCHAR(20),
	IN `rutNew` INT,
	IN `dvNew` VARCHAR(1),
	IN `nombrePrincipalNew` VARCHAR(45),
	IN `nombreSecundarioNew` VARCHAR(45),
	IN `apellidoPrincipalNew` VARCHAR(45),
	IN `apellidoSecundarioNew` VARCHAR(45),
	IN `fechaNacimientoNew` VARCHAR(45),
	IN `direccionNew` VARCHAR(60),
	IN `correoNew` VARCHAR(50),
	IN `numeroContactoNew` VARCHAR(20)
)
BEGIN
INSERT INTO usuario (NOMBRE_USUARIO, PASSWORD, RUT, DV, NOMBRE_PRINCIPAL, NOMBRE_SECUNDARIO, APELLIDO_PRINCIPAL, APELLIDO_SECUNDARIO, FECHA_NACIMIENTO, DIRECCION, CORREO, FECHA_ALTA, NUMERO_CONTACTO, TIPO_USUARIO) 
VALUES (usuarioNew, passwordNew,rutNew,dvNew,nombrePrincipalNew, nombreSecundarioNew, apellidoPrincipalNew, apellidoSecundarioNew, fechaNacimientoNew, direccionNew,correoNew,now(),numeroContactoNew, 'USUARIO');
END//
DELIMITER ;


DELIMITER //
CREATE PROCEDURE `sp_getUsuario`(
	IN `idUsuario` INT
)
BEGIN
SELECT * FROM usuario WHERE ID=idUsuario;
END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

-- Volcando estructura para procedimiento wait.sp_updateConfig
DELIMITER //
CREATE PROCEDURE `sp_updateConfig`(
	IN `llave` CHAR(50),
	IN `aforoNew` INT,
	IN `dotacionNew` INT,
	IN `atrasoNew` INT
)
BEGIN
UPDATE configuracion_socio SET AFORO=aforoNew, DOTACION_PERSONAL=dotacionNew, TIEMPO_ATRASO=atrasoNew WHERE PKCONFIG=llave;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_deleteConfig
DELIMITER //
CREATE PROCEDURE `sp_deleteConfig`(
	IN `idConfig` INT
)
BEGIN
DELETE FROM configuracion_socio WHERE ID = idConfig;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_deleteSucursal
DELIMITER //
CREATE PROCEDURE `sp_deleteSucursal`(
	IN `idSuc` INT
)
BEGIN
DELETE FROM sucursal WHERE ID = idSuc;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getAllConfig
DELIMITER //
CREATE PROCEDURE `sp_getAllConfig`()
BEGIN
SELECT * FROM configuracion_socio;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getAllSucursales
DELIMITER //
CREATE PROCEDURE `sp_getAllSucursales`()
BEGIN
SELECT * FROM sucursal;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getConfig
DELIMITER //
CREATE PROCEDURE `sp_getConfig`(
	IN `llaveConfig` CHAR(50)
)
BEGIN
SELECT * FROM configuracion_socio WHERE PKCONFIG=llaveConfig;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getDireccionSoc
DELIMITER //
CREATE PROCEDURE `sp_getDireccionSoc`(
	IN `llave` CHAR(50)
)
BEGIN
SELECT S.NOMBRE_SOCIO, '' AS NOMBRE_SUCURSAL, S.DIRECCION, S.NUMERO_CONTACTO, S.CORREO_CONTACTO FROM socio S WHERE S.PKCONFIG = llave;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getDireccionSuc
DELIMITER //
CREATE PROCEDURE `sp_getDireccionSuc`(
	IN `llave` CHAR(50)
)
BEGIN
SELECT SO.NOMBRE_SOCIO, SU.NOMBRE_SUCURSAL, SU.DIRECCION, SU.NUMERO_CONTACTO, SU.CORREO_CONTACTO FROM sucursal SU INNER JOIN SOCIO SO ON(SO.ID=SU.ID_SOCIO) WHERE SU.PKCONFIG = llave;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_getSucursal
DELIMITER //
CREATE PROCEDURE `sp_getSucursal`(
	IN `idSuc` INT
)
BEGIN
SELECT * FROM sucursal WHERE ID=idSuc;
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_insertConfig
DELIMITER //
CREATE PROCEDURE `sp_insertConfig`(
	IN `pkConfig` CHAR(50)
)
BEGIN
DECLARE aforoNew , dotacionNew , atrasoNew INTEGER DEFAULT 0;
SET aforoNew = 15;
SET dotacionNew = 4;
SET atrasoNew = 10;

INSERT INTO configuracion_socio (PKCONFIG,AFORO,DOTACION_PERSONAL,TIEMPO_ATRASO,CONFIG_HORARIO) VALUES(pkConfig,aforoNew,dotacionNew,atrasoNew,'{"dias": {"lunes": {"diahabil": true,"horainicio": "09:00.00","horatermino": "18:00.00"},"martes": {"diahabil": true,"horainicio": "09:00.00","horatermino": "18:00.00"},"miercoles": {"diahabil": true,"horainicio": "09:00.00","horatermino": "18:00.00"},"jueves": {"diahabil": true,"horainicio": "09:00.00","horatermino": "18:00.00"},"viernes": {"diahabil": true,"horainicio": "09:00.00","horatermino": "18:00.00"},"sabado": {"diahabil": false,"horainicio": "09:00.00","horatermino": "18:00.00"},"domingo": {"diahabil": false,"horainicio": "09:00.00","horatermino": "18:00.00"}}}');
END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_insertSocio
DELIMITER //
CREATE PROCEDURE `sp_insertSocio`(
	IN `nombreNew` VARCHAR(50),
	IN `rutNew` INT,
	IN `dvNew` VARCHAR(1),
	IN `direccionNew` VARCHAR(60),
	IN `numeroctoNew` VARCHAR(20),
	IN `numeroemerNew` VARCHAR(20),
	IN `correoNew` VARCHAR(50),
	IN `holdingNew` VARCHAR(50)
)
BEGIN

DECLARE idSocio INTEGER DEFAULT 0;
DECLARE pkConfig CHAR(50);
SELECT AUTO_INCREMENT INTO @idSocio FROM information_schema.TABLES WHERE TABLE_SCHEMA = "wait" AND TABLE_NAME="socio";
SET @pkConfig = CONCAT(CAST(@idSocio AS CHAR(50)), '.', '0');
INSERT INTO socio (NOMBRE_SOCIO, RUT, DV, DIRECCION, NUMERO_CONTACTO, NUMERO_EMERGENCIA, CORREO_CONTACTO, NOMBRE_HOLDING, PKCONFIG) 
VALUES (nombreNew,rutNew,dvNew,direccionNew,numeroctoNew,numeroemerNew,correoNew,holdingNew, @pkConfig);

END//
DELIMITER ;

-- Volcando estructura para procedimiento wait.sp_insertSucursal
DELIMITER //
CREATE PROCEDURE `sp_insertSucursal`(
	IN `idSocio` INT,
	IN `nombreNew` VARCHAR(45),
	IN `direccionNew` VARCHAR(50),
	IN `numeroNew` VARCHAR(20),
	IN `correoNew` VARCHAR(50)
)
BEGIN
DECLARE idSoc, idSucursal INTEGER DEFAULT 0;
DECLARE pkConfig CHAR(50);
SELECT S.ID INTO @idSoc FROM socio S WHERE S.ID=idSocio;
SELECT AUTO_INCREMENT INTO @idSucursal FROM information_schema.TABLES WHERE TABLE_SCHEMA = "wait" AND TABLE_NAME="sucursal";
SET @pkConfig = CONCAT(CAST(@idSoc AS CHAR(50)), '.', CAST(@idSucursal AS CHAR(50)));
INSERT INTO sucursal (ID_SOCIO, NOMBRE_SUCURSAL, DIRECCION, NUMERO_CONTACTO, CORREO_CONTACTO, ID_CONFIG) VALUES (@idSoc,nombreNew,direccionNew,numeroNew,correoNew, @pkConfig);
END//
DELIMITER ;
