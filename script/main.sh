#!/bin/bash
echo "----------------------------------------------"
echo "                 W A I T "
echo "----------------------------------------------"
echo "Script inicial de ambiente"

#Instalacion de dependencias
sh dependence_so/apt_install.sh

#Inicializacion base de datos mysql
echo "Inicializacion BBDD Mysql: wait"
sudo docker-compose -f dockerCompose/mysql/docker-compose.yml up --detach
echo "Inicializacion BBDD Mongodb: wait"
sudo docker-compose -f dockerCompose/mongo/docker-compose.yml up --detach

#Carga de script de bases de datos
sh ../bbdd/script/bbdd_main.sh